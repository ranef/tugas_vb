﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Frm_item
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Btn_delete_transaksi = New System.Windows.Forms.Button
        Me.Btn_Save = New System.Windows.Forms.Button
        Me.Btn_Print = New System.Windows.Forms.Button
        Me.Btn_New = New System.Windows.Forms.Button
        Me.dg_item = New System.Windows.Forms.DataGridView
        Me.Txt_item_code = New System.Windows.Forms.TextBox
        Me.Lbl_item_name = New System.Windows.Forms.Label
        Me.Lbl_item_code = New System.Windows.Forms.Label
        Me.LblItem = New System.Windows.Forms.Label
        Me.Txt_price = New System.Windows.Forms.TextBox
        Me.Lbl_price = New System.Windows.Forms.Label
        Me.Btn_item_code = New System.Windows.Forms.Button
        Me.Txt_item_name = New System.Windows.Forms.TextBox
        CType(Me.dg_item, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Btn_delete_transaksi
        '
        Me.Btn_delete_transaksi.Location = New System.Drawing.Point(158, 407)
        Me.Btn_delete_transaksi.Name = "Btn_delete_transaksi"
        Me.Btn_delete_transaksi.Size = New System.Drawing.Size(103, 23)
        Me.Btn_delete_transaksi.TabIndex = 19
        Me.Btn_delete_transaksi.Text = "Delete Transaksi"
        Me.Btn_delete_transaksi.UseVisualStyleBackColor = True
        '
        'Btn_Save
        '
        Me.Btn_Save.Location = New System.Drawing.Point(158, 373)
        Me.Btn_Save.Name = "Btn_Save"
        Me.Btn_Save.Size = New System.Drawing.Size(103, 23)
        Me.Btn_Save.TabIndex = 18
        Me.Btn_Save.Text = "Save"
        Me.Btn_Save.UseVisualStyleBackColor = True
        '
        'Btn_Print
        '
        Me.Btn_Print.Location = New System.Drawing.Point(41, 407)
        Me.Btn_Print.Name = "Btn_Print"
        Me.Btn_Print.Size = New System.Drawing.Size(98, 23)
        Me.Btn_Print.TabIndex = 17
        Me.Btn_Print.Text = "Print"
        Me.Btn_Print.UseVisualStyleBackColor = True
        '
        'Btn_New
        '
        Me.Btn_New.Location = New System.Drawing.Point(41, 373)
        Me.Btn_New.Name = "Btn_New"
        Me.Btn_New.Size = New System.Drawing.Size(98, 23)
        Me.Btn_New.TabIndex = 16
        Me.Btn_New.Text = "New"
        Me.Btn_New.UseVisualStyleBackColor = True
        '
        'dg_item
        '
        Me.dg_item.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dg_item.Location = New System.Drawing.Point(41, 129)
        Me.dg_item.Name = "dg_item"
        Me.dg_item.Size = New System.Drawing.Size(618, 232)
        Me.dg_item.TabIndex = 15
        '
        'Txt_item_code
        '
        Me.Txt_item_code.Location = New System.Drawing.Point(135, 56)
        Me.Txt_item_code.Name = "Txt_item_code"
        Me.Txt_item_code.Size = New System.Drawing.Size(158, 20)
        Me.Txt_item_code.TabIndex = 13
        '
        'Lbl_item_name
        '
        Me.Lbl_item_name.AutoSize = True
        Me.Lbl_item_name.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Lbl_item_name.Location = New System.Drawing.Point(38, 91)
        Me.Lbl_item_name.Name = "Lbl_item_name"
        Me.Lbl_item_name.Size = New System.Drawing.Size(80, 18)
        Me.Lbl_item_name.TabIndex = 12
        Me.Lbl_item_name.Text = "Item Name"
        '
        'Lbl_item_code
        '
        Me.Lbl_item_code.AutoSize = True
        Me.Lbl_item_code.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Lbl_item_code.Location = New System.Drawing.Point(38, 56)
        Me.Lbl_item_code.Name = "Lbl_item_code"
        Me.Lbl_item_code.Size = New System.Drawing.Size(76, 18)
        Me.Lbl_item_code.TabIndex = 11
        Me.Lbl_item_code.Text = "Item Code"
        '
        'LblItem
        '
        Me.LblItem.AutoSize = True
        Me.LblItem.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblItem.Location = New System.Drawing.Point(316, 10)
        Me.LblItem.Name = "LblItem"
        Me.LblItem.Size = New System.Drawing.Size(56, 25)
        Me.LblItem.TabIndex = 10
        Me.LblItem.Text = "Item"
        '
        'Txt_price
        '
        Me.Txt_price.Location = New System.Drawing.Point(484, 56)
        Me.Txt_price.Name = "Txt_price"
        Me.Txt_price.Size = New System.Drawing.Size(175, 20)
        Me.Txt_price.TabIndex = 20
        '
        'Lbl_price
        '
        Me.Lbl_price.AutoSize = True
        Me.Lbl_price.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Lbl_price.Location = New System.Drawing.Point(432, 56)
        Me.Lbl_price.Name = "Lbl_price"
        Me.Lbl_price.Size = New System.Drawing.Size(42, 18)
        Me.Lbl_price.TabIndex = 21
        Me.Lbl_price.Text = "Price"
        '
        'Btn_item_code
        '
        Me.Btn_item_code.Location = New System.Drawing.Point(298, 54)
        Me.Btn_item_code.Name = "Btn_item_code"
        Me.Btn_item_code.Size = New System.Drawing.Size(48, 23)
        Me.Btn_item_code.TabIndex = 22
        Me.Btn_item_code.Text = "..."
        Me.Btn_item_code.UseVisualStyleBackColor = True
        '
        'Txt_item_name
        '
        Me.Txt_item_name.Location = New System.Drawing.Point(135, 89)
        Me.Txt_item_name.Name = "Txt_item_name"
        Me.Txt_item_name.Size = New System.Drawing.Size(211, 20)
        Me.Txt_item_name.TabIndex = 23
        '
        'Frm_item
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(690, 447)
        Me.Controls.Add(Me.Txt_item_name)
        Me.Controls.Add(Me.Btn_item_code)
        Me.Controls.Add(Me.Lbl_price)
        Me.Controls.Add(Me.Txt_price)
        Me.Controls.Add(Me.Btn_delete_transaksi)
        Me.Controls.Add(Me.Btn_Save)
        Me.Controls.Add(Me.Btn_Print)
        Me.Controls.Add(Me.Btn_New)
        Me.Controls.Add(Me.dg_item)
        Me.Controls.Add(Me.Txt_item_code)
        Me.Controls.Add(Me.Lbl_item_name)
        Me.Controls.Add(Me.Lbl_item_code)
        Me.Controls.Add(Me.LblItem)
        Me.Name = "Frm_item"
        Me.Text = "Form Item"
        CType(Me.dg_item, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Btn_delete_transaksi As System.Windows.Forms.Button
    Friend WithEvents Btn_Save As System.Windows.Forms.Button
    Friend WithEvents Btn_Print As System.Windows.Forms.Button
    Friend WithEvents Btn_New As System.Windows.Forms.Button
    Friend WithEvents dg_item As System.Windows.Forms.DataGridView
    Friend WithEvents Txt_item_code As System.Windows.Forms.TextBox
    Friend WithEvents Lbl_item_name As System.Windows.Forms.Label
    Friend WithEvents Lbl_item_code As System.Windows.Forms.Label
    Friend WithEvents LblItem As System.Windows.Forms.Label
    Friend WithEvents Txt_price As System.Windows.Forms.TextBox
    Friend WithEvents Lbl_price As System.Windows.Forms.Label
    Friend WithEvents Btn_item_code As System.Windows.Forms.Button
    Friend WithEvents Txt_item_name As System.Windows.Forms.TextBox
End Class
