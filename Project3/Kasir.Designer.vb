﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmKasir
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.LblPenjualan = New System.Windows.Forms.Label
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.Pnl1 = New System.Windows.Forms.Panel
        Me.BtnClose = New System.Windows.Forms.Button
        Me.BtnHapus = New System.Windows.Forms.Button
        Me.BtnSimpan = New System.Windows.Forms.Button
        Me.BtnBatal = New System.Windows.Forms.Button
        Me.BtnTambah = New System.Windows.Forms.Button
        Me.BtnCari = New System.Windows.Forms.Button
        Me.Button6 = New System.Windows.Forms.Button
        Me.Button5 = New System.Windows.Forms.Button
        Me.Button2 = New System.Windows.Forms.Button
        Me.Button4 = New System.Windows.Forms.Button
        Me.Button1 = New System.Windows.Forms.Button
        Me.Pnl4 = New System.Windows.Forms.Panel
        Me.TxtKembali = New System.Windows.Forms.TextBox
        Me.TxtGrandtotal = New System.Windows.Forms.TextBox
        Me.LblKembali = New System.Windows.Forms.Label
        Me.LblGrandTotal = New System.Windows.Forms.Label
        Me.Pnl2 = New System.Windows.Forms.Panel
        Me.BtnCariCustomerCode = New System.Windows.Forms.Button
        Me.TxtCustomerName = New System.Windows.Forms.TextBox
        Me.LblCustomerName = New System.Windows.Forms.Label
        Me.TxtCustomerCode = New System.Windows.Forms.TextBox
        Me.LblCustomerCode = New System.Windows.Forms.Label
        Me.dtpTgl = New System.Windows.Forms.DateTimePicker
        Me.LblTgl = New System.Windows.Forms.Label
        Me.TxtNoStruk = New System.Windows.Forms.TextBox
        Me.LblNoStruk = New System.Windows.Forms.Label
        Me.LblItemCode = New System.Windows.Forms.Label
        Me.LblItemName = New System.Windows.Forms.Label
        Me.LblPrice = New System.Windows.Forms.Label
        Me.LblQty = New System.Windows.Forms.Label
        Me.LblSubtotalDetail = New System.Windows.Forms.Label
        Me.TxtItemCode = New System.Windows.Forms.TextBox
        Me.BtnCariItemCode = New System.Windows.Forms.Button
        Me.TxtItemName = New System.Windows.Forms.TextBox
        Me.TxtPrice = New System.Windows.Forms.TextBox
        Me.TxtQty = New System.Windows.Forms.TextBox
        Me.TxtSubtotalDetail = New System.Windows.Forms.TextBox
        Me.DgPenjualan = New System.Windows.Forms.DataGridView
        Me.LblSubtotalHeader = New System.Windows.Forms.Label
        Me.TxtSubtotalHeader = New System.Windows.Forms.TextBox
        Me.LblPpn = New System.Windows.Forms.Label
        Me.TxtPpn = New System.Windows.Forms.TextBox
        Me.LblBayar = New System.Windows.Forms.Label
        Me.TxtBayar = New System.Windows.Forms.TextBox
        Me.Pnl3 = New System.Windows.Forms.Panel
        Me.BtnCetak = New System.Windows.Forms.Button
        Me.Panel1.SuspendLayout()
        Me.Pnl1.SuspendLayout()
        Me.Pnl4.SuspendLayout()
        Me.Pnl2.SuspendLayout()
        CType(Me.DgPenjualan, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Pnl3.SuspendLayout()
        Me.SuspendLayout()
        '
        'LblPenjualan
        '
        Me.LblPenjualan.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.LblPenjualan.Dock = System.Windows.Forms.DockStyle.Top
        Me.LblPenjualan.Font = New System.Drawing.Font("Modern No. 20", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblPenjualan.Location = New System.Drawing.Point(0, 0)
        Me.LblPenjualan.Name = "LblPenjualan"
        Me.LblPenjualan.Size = New System.Drawing.Size(560, 48)
        Me.LblPenjualan.TabIndex = 0
        Me.LblPenjualan.Text = "PENJUALAN"
        Me.LblPenjualan.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.Pnl1)
        Me.Panel1.Controls.Add(Me.Button6)
        Me.Panel1.Controls.Add(Me.Button5)
        Me.Panel1.Controls.Add(Me.Button2)
        Me.Panel1.Controls.Add(Me.Button4)
        Me.Panel1.Controls.Add(Me.Button1)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.Location = New System.Drawing.Point(0, 48)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(560, 88)
        Me.Panel1.TabIndex = 1
        '
        'Pnl1
        '
        Me.Pnl1.Controls.Add(Me.BtnCetak)
        Me.Pnl1.Controls.Add(Me.BtnClose)
        Me.Pnl1.Controls.Add(Me.BtnHapus)
        Me.Pnl1.Controls.Add(Me.BtnSimpan)
        Me.Pnl1.Controls.Add(Me.BtnBatal)
        Me.Pnl1.Controls.Add(Me.BtnTambah)
        Me.Pnl1.Controls.Add(Me.BtnCari)
        Me.Pnl1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Pnl1.Location = New System.Drawing.Point(0, 0)
        Me.Pnl1.Name = "Pnl1"
        Me.Pnl1.Size = New System.Drawing.Size(560, 88)
        Me.Pnl1.TabIndex = 9
        '
        'BtnClose
        '
        Me.BtnClose.Location = New System.Drawing.Point(491, 12)
        Me.BtnClose.Name = "BtnClose"
        Me.BtnClose.Size = New System.Drawing.Size(60, 64)
        Me.BtnClose.TabIndex = 8
        Me.BtnClose.Text = "&Close"
        Me.BtnClose.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.BtnClose.UseVisualStyleBackColor = True
        '
        'BtnHapus
        '
        Me.BtnHapus.Location = New System.Drawing.Point(271, 12)
        Me.BtnHapus.Name = "BtnHapus"
        Me.BtnHapus.Size = New System.Drawing.Size(60, 64)
        Me.BtnHapus.TabIndex = 7
        Me.BtnHapus.Text = "&Hapus"
        Me.BtnHapus.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.BtnHapus.UseVisualStyleBackColor = True
        '
        'BtnSimpan
        '
        Me.BtnSimpan.Location = New System.Drawing.Point(140, 12)
        Me.BtnSimpan.Name = "BtnSimpan"
        Me.BtnSimpan.Size = New System.Drawing.Size(60, 64)
        Me.BtnSimpan.TabIndex = 6
        Me.BtnSimpan.Text = "&Simpan"
        Me.BtnSimpan.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.BtnSimpan.UseVisualStyleBackColor = True
        '
        'BtnBatal
        '
        Me.BtnBatal.Location = New System.Drawing.Point(204, 12)
        Me.BtnBatal.Name = "BtnBatal"
        Me.BtnBatal.Size = New System.Drawing.Size(60, 64)
        Me.BtnBatal.TabIndex = 5
        Me.BtnBatal.Text = "&Batal"
        Me.BtnBatal.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.BtnBatal.UseVisualStyleBackColor = True
        '
        'BtnTambah
        '
        Me.BtnTambah.Location = New System.Drawing.Point(76, 12)
        Me.BtnTambah.Name = "BtnTambah"
        Me.BtnTambah.Size = New System.Drawing.Size(60, 64)
        Me.BtnTambah.TabIndex = 4
        Me.BtnTambah.Text = "&Tambah"
        Me.BtnTambah.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.BtnTambah.UseVisualStyleBackColor = True
        '
        'BtnCari
        '
        Me.BtnCari.ImageAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.BtnCari.Location = New System.Drawing.Point(12, 12)
        Me.BtnCari.Name = "BtnCari"
        Me.BtnCari.Size = New System.Drawing.Size(60, 64)
        Me.BtnCari.TabIndex = 3
        Me.BtnCari.Text = "&Cari"
        Me.BtnCari.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.BtnCari.UseVisualStyleBackColor = True
        '
        'Button6
        '
        Me.Button6.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button6.Location = New System.Drawing.Point(499, 12)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(49, 64)
        Me.Button6.TabIndex = 8
        Me.Button6.Text = "Button2"
        Me.Button6.UseVisualStyleBackColor = True
        '
        'Button5
        '
        Me.Button5.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button5.Location = New System.Drawing.Point(228, 12)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(49, 64)
        Me.Button5.TabIndex = 7
        Me.Button5.Text = "Button2"
        Me.Button5.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button2.Location = New System.Drawing.Point(120, 12)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(49, 64)
        Me.Button2.TabIndex = 6
        Me.Button2.Text = "Button2"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Button4
        '
        Me.Button4.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button4.Location = New System.Drawing.Point(174, 12)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(49, 64)
        Me.Button4.TabIndex = 5
        Me.Button4.Text = "Button4"
        Me.Button4.UseVisualStyleBackColor = True
        '
        'Button1
        '
        Me.Button1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button1.Location = New System.Drawing.Point(12, 12)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(49, 64)
        Me.Button1.TabIndex = 3
        Me.Button1.Text = "Button1"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Pnl4
        '
        Me.Pnl4.BackColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Pnl4.Controls.Add(Me.TxtKembali)
        Me.Pnl4.Controls.Add(Me.TxtGrandtotal)
        Me.Pnl4.Controls.Add(Me.LblKembali)
        Me.Pnl4.Controls.Add(Me.LblGrandTotal)
        Me.Pnl4.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Pnl4.Location = New System.Drawing.Point(0, 412)
        Me.Pnl4.Name = "Pnl4"
        Me.Pnl4.Size = New System.Drawing.Size(560, 108)
        Me.Pnl4.TabIndex = 2
        '
        'TxtKembali
        '
        Me.TxtKembali.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TxtKembali.BackColor = System.Drawing.Color.Black
        Me.TxtKembali.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TxtKembali.Font = New System.Drawing.Font("Microsoft Sans Serif", 26.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtKembali.ForeColor = System.Drawing.Color.White
        Me.TxtKembali.Location = New System.Drawing.Point(291, 32)
        Me.TxtKembali.Multiline = True
        Me.TxtKembali.Name = "TxtKembali"
        Me.TxtKembali.ReadOnly = True
        Me.TxtKembali.Size = New System.Drawing.Size(260, 66)
        Me.TxtKembali.TabIndex = 25
        Me.TxtKembali.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'TxtGrandtotal
        '
        Me.TxtGrandtotal.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TxtGrandtotal.BackColor = System.Drawing.Color.Black
        Me.TxtGrandtotal.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TxtGrandtotal.Font = New System.Drawing.Font("Microsoft Sans Serif", 26.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtGrandtotal.ForeColor = System.Drawing.Color.White
        Me.TxtGrandtotal.Location = New System.Drawing.Point(18, 32)
        Me.TxtGrandtotal.Multiline = True
        Me.TxtGrandtotal.Name = "TxtGrandtotal"
        Me.TxtGrandtotal.ReadOnly = True
        Me.TxtGrandtotal.Size = New System.Drawing.Size(262, 66)
        Me.TxtGrandtotal.TabIndex = 23
        Me.TxtGrandtotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'LblKembali
        '
        Me.LblKembali.BackColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.LblKembali.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblKembali.ForeColor = System.Drawing.SystemColors.ButtonFace
        Me.LblKembali.Location = New System.Drawing.Point(291, 3)
        Me.LblKembali.Name = "LblKembali"
        Me.LblKembali.Size = New System.Drawing.Size(260, 26)
        Me.LblKembali.TabIndex = 24
        Me.LblKembali.Text = "Kembali"
        Me.LblKembali.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'LblGrandTotal
        '
        Me.LblGrandTotal.BackColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.LblGrandTotal.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblGrandTotal.ForeColor = System.Drawing.SystemColors.ButtonFace
        Me.LblGrandTotal.Location = New System.Drawing.Point(18, 3)
        Me.LblGrandTotal.Name = "LblGrandTotal"
        Me.LblGrandTotal.Size = New System.Drawing.Size(262, 26)
        Me.LblGrandTotal.TabIndex = 23
        Me.LblGrandTotal.Text = "Grandtotal"
        Me.LblGrandTotal.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Pnl2
        '
        Me.Pnl2.Controls.Add(Me.BtnCariCustomerCode)
        Me.Pnl2.Controls.Add(Me.TxtCustomerName)
        Me.Pnl2.Controls.Add(Me.LblCustomerName)
        Me.Pnl2.Controls.Add(Me.TxtCustomerCode)
        Me.Pnl2.Controls.Add(Me.LblCustomerCode)
        Me.Pnl2.Controls.Add(Me.dtpTgl)
        Me.Pnl2.Controls.Add(Me.LblTgl)
        Me.Pnl2.Controls.Add(Me.TxtNoStruk)
        Me.Pnl2.Controls.Add(Me.LblNoStruk)
        Me.Pnl2.Dock = System.Windows.Forms.DockStyle.Top
        Me.Pnl2.Location = New System.Drawing.Point(0, 136)
        Me.Pnl2.Name = "Pnl2"
        Me.Pnl2.Size = New System.Drawing.Size(560, 70)
        Me.Pnl2.TabIndex = 3
        '
        'BtnCariCustomerCode
        '
        Me.BtnCariCustomerCode.Location = New System.Drawing.Point(528, 10)
        Me.BtnCariCustomerCode.Name = "BtnCariCustomerCode"
        Me.BtnCariCustomerCode.Size = New System.Drawing.Size(24, 23)
        Me.BtnCariCustomerCode.TabIndex = 8
        Me.BtnCariCustomerCode.Text = "..."
        Me.BtnCariCustomerCode.UseVisualStyleBackColor = True
        '
        'TxtCustomerName
        '
        Me.TxtCustomerName.Location = New System.Drawing.Point(369, 38)
        Me.TxtCustomerName.Name = "TxtCustomerName"
        Me.TxtCustomerName.ReadOnly = True
        Me.TxtCustomerName.Size = New System.Drawing.Size(153, 20)
        Me.TxtCustomerName.TabIndex = 7
        '
        'LblCustomerName
        '
        Me.LblCustomerName.AutoSize = True
        Me.LblCustomerName.Location = New System.Drawing.Point(288, 40)
        Me.LblCustomerName.Name = "LblCustomerName"
        Me.LblCustomerName.Size = New System.Drawing.Size(82, 13)
        Me.LblCustomerName.TabIndex = 6
        Me.LblCustomerName.Text = "Customer Name"
        '
        'TxtCustomerCode
        '
        Me.TxtCustomerCode.Location = New System.Drawing.Point(369, 12)
        Me.TxtCustomerCode.Name = "TxtCustomerCode"
        Me.TxtCustomerCode.Size = New System.Drawing.Size(153, 20)
        Me.TxtCustomerCode.TabIndex = 5
        '
        'LblCustomerCode
        '
        Me.LblCustomerCode.AutoSize = True
        Me.LblCustomerCode.Location = New System.Drawing.Point(288, 14)
        Me.LblCustomerCode.Name = "LblCustomerCode"
        Me.LblCustomerCode.Size = New System.Drawing.Size(79, 13)
        Me.LblCustomerCode.TabIndex = 4
        Me.LblCustomerCode.Text = "Customer Code"
        '
        'dtpTgl
        '
        Me.dtpTgl.Location = New System.Drawing.Point(71, 39)
        Me.dtpTgl.Name = "dtpTgl"
        Me.dtpTgl.Size = New System.Drawing.Size(153, 20)
        Me.dtpTgl.TabIndex = 3
        '
        'LblTgl
        '
        Me.LblTgl.AutoSize = True
        Me.LblTgl.Location = New System.Drawing.Point(12, 40)
        Me.LblTgl.Name = "LblTgl"
        Me.LblTgl.Size = New System.Drawing.Size(22, 13)
        Me.LblTgl.TabIndex = 2
        Me.LblTgl.Text = "Tgl"
        '
        'TxtNoStruk
        '
        Me.TxtNoStruk.Location = New System.Drawing.Point(71, 12)
        Me.TxtNoStruk.Name = "TxtNoStruk"
        Me.TxtNoStruk.Size = New System.Drawing.Size(153, 20)
        Me.TxtNoStruk.TabIndex = 1
        '
        'LblNoStruk
        '
        Me.LblNoStruk.AutoSize = True
        Me.LblNoStruk.Location = New System.Drawing.Point(12, 14)
        Me.LblNoStruk.Name = "LblNoStruk"
        Me.LblNoStruk.Size = New System.Drawing.Size(52, 13)
        Me.LblNoStruk.TabIndex = 0
        Me.LblNoStruk.Text = "No. Struk"
        '
        'LblItemCode
        '
        Me.LblItemCode.BackColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.LblItemCode.ForeColor = System.Drawing.SystemColors.ButtonFace
        Me.LblItemCode.Location = New System.Drawing.Point(15, 209)
        Me.LblItemCode.Name = "LblItemCode"
        Me.LblItemCode.Size = New System.Drawing.Size(83, 14)
        Me.LblItemCode.TabIndex = 9
        Me.LblItemCode.Text = "Item Code"
        Me.LblItemCode.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'LblItemName
        '
        Me.LblItemName.BackColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.LblItemName.ForeColor = System.Drawing.SystemColors.ButtonFace
        Me.LblItemName.Location = New System.Drawing.Point(129, 209)
        Me.LblItemName.Name = "LblItemName"
        Me.LblItemName.Size = New System.Drawing.Size(161, 13)
        Me.LblItemName.TabIndex = 10
        Me.LblItemName.Text = "ItemName"
        Me.LblItemName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'LblPrice
        '
        Me.LblPrice.BackColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.LblPrice.ForeColor = System.Drawing.SystemColors.ButtonFace
        Me.LblPrice.Location = New System.Drawing.Point(294, 209)
        Me.LblPrice.Name = "LblPrice"
        Me.LblPrice.Size = New System.Drawing.Size(106, 14)
        Me.LblPrice.TabIndex = 11
        Me.LblPrice.Text = "Price"
        Me.LblPrice.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'LblQty
        '
        Me.LblQty.BackColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.LblQty.ForeColor = System.Drawing.SystemColors.ButtonFace
        Me.LblQty.Location = New System.Drawing.Point(403, 210)
        Me.LblQty.Name = "LblQty"
        Me.LblQty.Size = New System.Drawing.Size(48, 13)
        Me.LblQty.TabIndex = 12
        Me.LblQty.Text = "Qty"
        Me.LblQty.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'LblSubtotalDetail
        '
        Me.LblSubtotalDetail.BackColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.LblSubtotalDetail.ForeColor = System.Drawing.SystemColors.ButtonFace
        Me.LblSubtotalDetail.Location = New System.Drawing.Point(457, 209)
        Me.LblSubtotalDetail.Name = "LblSubtotalDetail"
        Me.LblSubtotalDetail.Size = New System.Drawing.Size(95, 14)
        Me.LblSubtotalDetail.TabIndex = 13
        Me.LblSubtotalDetail.Text = "Subtotal Detail"
        Me.LblSubtotalDetail.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'TxtItemCode
        '
        Me.TxtItemCode.Location = New System.Drawing.Point(15, 225)
        Me.TxtItemCode.Name = "TxtItemCode"
        Me.TxtItemCode.Size = New System.Drawing.Size(83, 20)
        Me.TxtItemCode.TabIndex = 9
        '
        'BtnCariItemCode
        '
        Me.BtnCariItemCode.Location = New System.Drawing.Point(100, 225)
        Me.BtnCariItemCode.Name = "BtnCariItemCode"
        Me.BtnCariItemCode.Size = New System.Drawing.Size(26, 23)
        Me.BtnCariItemCode.TabIndex = 9
        Me.BtnCariItemCode.Text = "..."
        Me.BtnCariItemCode.UseVisualStyleBackColor = True
        '
        'TxtItemName
        '
        Me.TxtItemName.Location = New System.Drawing.Point(129, 225)
        Me.TxtItemName.Name = "TxtItemName"
        Me.TxtItemName.ReadOnly = True
        Me.TxtItemName.Size = New System.Drawing.Size(159, 20)
        Me.TxtItemName.TabIndex = 14
        '
        'TxtPrice
        '
        Me.TxtPrice.Location = New System.Drawing.Point(294, 225)
        Me.TxtPrice.Name = "TxtPrice"
        Me.TxtPrice.ReadOnly = True
        Me.TxtPrice.Size = New System.Drawing.Size(106, 20)
        Me.TxtPrice.TabIndex = 15
        '
        'TxtQty
        '
        Me.TxtQty.Location = New System.Drawing.Point(403, 225)
        Me.TxtQty.Name = "TxtQty"
        Me.TxtQty.Size = New System.Drawing.Size(48, 20)
        Me.TxtQty.TabIndex = 16
        '
        'TxtSubtotalDetail
        '
        Me.TxtSubtotalDetail.Location = New System.Drawing.Point(457, 225)
        Me.TxtSubtotalDetail.Name = "TxtSubtotalDetail"
        Me.TxtSubtotalDetail.Size = New System.Drawing.Size(94, 20)
        Me.TxtSubtotalDetail.TabIndex = 17
        '
        'DgPenjualan
        '
        Me.DgPenjualan.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DgPenjualan.Location = New System.Drawing.Point(18, 252)
        Me.DgPenjualan.Name = "DgPenjualan"
        Me.DgPenjualan.Size = New System.Drawing.Size(534, 117)
        Me.DgPenjualan.TabIndex = 18
        '
        'LblSubtotalHeader
        '
        Me.LblSubtotalHeader.AutoSize = True
        Me.LblSubtotalHeader.Location = New System.Drawing.Point(15, 385)
        Me.LblSubtotalHeader.Name = "LblSubtotalHeader"
        Me.LblSubtotalHeader.Size = New System.Drawing.Size(84, 13)
        Me.LblSubtotalHeader.TabIndex = 9
        Me.LblSubtotalHeader.Text = "Subtotal Header"
        '
        'TxtSubtotalHeader
        '
        Me.TxtSubtotalHeader.Location = New System.Drawing.Point(100, 382)
        Me.TxtSubtotalHeader.Name = "TxtSubtotalHeader"
        Me.TxtSubtotalHeader.ReadOnly = True
        Me.TxtSubtotalHeader.Size = New System.Drawing.Size(96, 20)
        Me.TxtSubtotalHeader.TabIndex = 9
        '
        'LblPpn
        '
        Me.LblPpn.AutoSize = True
        Me.LblPpn.Location = New System.Drawing.Point(222, 385)
        Me.LblPpn.Name = "LblPpn"
        Me.LblPpn.Size = New System.Drawing.Size(29, 13)
        Me.LblPpn.TabIndex = 19
        Me.LblPpn.Text = "PPN"
        '
        'TxtPpn
        '
        Me.TxtPpn.Location = New System.Drawing.Point(257, 383)
        Me.TxtPpn.Name = "TxtPpn"
        Me.TxtPpn.ReadOnly = True
        Me.TxtPpn.Size = New System.Drawing.Size(96, 20)
        Me.TxtPpn.TabIndex = 20
        '
        'LblBayar
        '
        Me.LblBayar.AutoSize = True
        Me.LblBayar.Location = New System.Drawing.Point(368, 385)
        Me.LblBayar.Name = "LblBayar"
        Me.LblBayar.Size = New System.Drawing.Size(34, 13)
        Me.LblBayar.TabIndex = 21
        Me.LblBayar.Text = "Bayar"
        '
        'TxtBayar
        '
        Me.TxtBayar.Location = New System.Drawing.Point(403, 383)
        Me.TxtBayar.Name = "TxtBayar"
        Me.TxtBayar.Size = New System.Drawing.Size(148, 20)
        Me.TxtBayar.TabIndex = 22
        '
        'Pnl3
        '
        Me.Pnl3.Controls.Add(Me.TxtBayar)
        Me.Pnl3.Controls.Add(Me.LblBayar)
        Me.Pnl3.Controls.Add(Me.TxtPpn)
        Me.Pnl3.Controls.Add(Me.LblPpn)
        Me.Pnl3.Controls.Add(Me.TxtSubtotalHeader)
        Me.Pnl3.Controls.Add(Me.LblSubtotalHeader)
        Me.Pnl3.Controls.Add(Me.DgPenjualan)
        Me.Pnl3.Controls.Add(Me.TxtSubtotalDetail)
        Me.Pnl3.Controls.Add(Me.TxtQty)
        Me.Pnl3.Controls.Add(Me.TxtPrice)
        Me.Pnl3.Controls.Add(Me.TxtItemName)
        Me.Pnl3.Controls.Add(Me.BtnCariItemCode)
        Me.Pnl3.Controls.Add(Me.TxtItemCode)
        Me.Pnl3.Controls.Add(Me.LblSubtotalDetail)
        Me.Pnl3.Controls.Add(Me.LblQty)
        Me.Pnl3.Controls.Add(Me.LblPrice)
        Me.Pnl3.Controls.Add(Me.LblItemName)
        Me.Pnl3.Controls.Add(Me.LblItemCode)
        Me.Pnl3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Pnl3.Location = New System.Drawing.Point(0, 0)
        Me.Pnl3.Name = "Pnl3"
        Me.Pnl3.Size = New System.Drawing.Size(560, 520)
        Me.Pnl3.TabIndex = 2
        '
        'BtnCetak
        '
        Me.BtnCetak.Location = New System.Drawing.Point(340, 12)
        Me.BtnCetak.Name = "BtnCetak"
        Me.BtnCetak.Size = New System.Drawing.Size(60, 64)
        Me.BtnCetak.TabIndex = 9
        Me.BtnCetak.Text = "&Cetak"
        Me.BtnCetak.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.BtnCetak.UseVisualStyleBackColor = True
        '
        'FrmKasir
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(560, 520)
        Me.Controls.Add(Me.Pnl2)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.LblPenjualan)
        Me.Controls.Add(Me.Pnl4)
        Me.Controls.Add(Me.Pnl3)
        Me.Name = "FrmKasir"
        Me.Text = "Kasir"
        Me.Panel1.ResumeLayout(False)
        Me.Pnl1.ResumeLayout(False)
        Me.Pnl4.ResumeLayout(False)
        Me.Pnl4.PerformLayout()
        Me.Pnl2.ResumeLayout(False)
        Me.Pnl2.PerformLayout()
        CType(Me.DgPenjualan, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Pnl3.ResumeLayout(False)
        Me.Pnl3.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents LblPenjualan As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Pnl1 As System.Windows.Forms.Panel
    Friend WithEvents BtnHapus As System.Windows.Forms.Button
    Friend WithEvents BtnSimpan As System.Windows.Forms.Button
    Friend WithEvents BtnBatal As System.Windows.Forms.Button
    Friend WithEvents BtnTambah As System.Windows.Forms.Button
    Friend WithEvents BtnCari As System.Windows.Forms.Button
    Friend WithEvents Button6 As System.Windows.Forms.Button
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents Pnl4 As System.Windows.Forms.Panel
    Friend WithEvents Pnl2 As System.Windows.Forms.Panel
    Friend WithEvents LblNoStruk As System.Windows.Forms.Label
    Friend WithEvents BtnCariCustomerCode As System.Windows.Forms.Button
    Friend WithEvents TxtCustomerName As System.Windows.Forms.TextBox
    Friend WithEvents LblCustomerName As System.Windows.Forms.Label
    Friend WithEvents TxtCustomerCode As System.Windows.Forms.TextBox
    Friend WithEvents LblCustomerCode As System.Windows.Forms.Label
    Friend WithEvents dtpTgl As System.Windows.Forms.DateTimePicker
    Friend WithEvents LblTgl As System.Windows.Forms.Label
    Friend WithEvents TxtNoStruk As System.Windows.Forms.TextBox
    Friend WithEvents TxtKembali As System.Windows.Forms.TextBox
    Friend WithEvents TxtGrandtotal As System.Windows.Forms.TextBox
    Friend WithEvents LblKembali As System.Windows.Forms.Label
    Friend WithEvents LblGrandTotal As System.Windows.Forms.Label
    Friend WithEvents BtnClose As System.Windows.Forms.Button
    Friend WithEvents LblItemCode As System.Windows.Forms.Label
    Friend WithEvents LblItemName As System.Windows.Forms.Label
    Friend WithEvents LblPrice As System.Windows.Forms.Label
    Friend WithEvents LblQty As System.Windows.Forms.Label
    Friend WithEvents LblSubtotalDetail As System.Windows.Forms.Label
    Friend WithEvents TxtItemCode As System.Windows.Forms.TextBox
    Friend WithEvents BtnCariItemCode As System.Windows.Forms.Button
    Friend WithEvents TxtItemName As System.Windows.Forms.TextBox
    Friend WithEvents TxtPrice As System.Windows.Forms.TextBox
    Friend WithEvents TxtQty As System.Windows.Forms.TextBox
    Friend WithEvents TxtSubtotalDetail As System.Windows.Forms.TextBox
    Friend WithEvents DgPenjualan As System.Windows.Forms.DataGridView
    Friend WithEvents LblSubtotalHeader As System.Windows.Forms.Label
    Friend WithEvents TxtSubtotalHeader As System.Windows.Forms.TextBox
    Friend WithEvents LblPpn As System.Windows.Forms.Label
    Friend WithEvents TxtPpn As System.Windows.Forms.TextBox
    Friend WithEvents LblBayar As System.Windows.Forms.Label
    Friend WithEvents TxtBayar As System.Windows.Forms.TextBox
    Friend WithEvents Pnl3 As System.Windows.Forms.Panel
    Friend WithEvents BtnCetak As System.Windows.Forms.Button
End Class
