﻿Public Class FrmKasir

    Private Sub FrmKasir_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub

    Private Sub TxtGrandtotal_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TxtGrandtotal.TextChanged

    End Sub

    Private Sub BtnTambah_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnTambah.Click
        '181124004 <<< contoh nomer struk
        Dim ds As DataSet
        Dim nostruk As String
        ds = QueryToDataSet("select * from t_struk where " & _
                            "tgl=curdate() order by struk_no desc")
        If ds.Tables(0).Rows.Count > 0 Then
            'ada data
            nostruk = ds.Tables(0).Rows(0).Item("struk_no").ToString
            nostruk = Microsoft.VisualBasic.Right(nostruk, 3) + 1
            'nostruk = Mid(nostruk, 7, 3) + 1  <<<(hanya pada vb) 
            nostruk = Format(CInt(nostruk), "00#")
            'tanda # untuk menunjukan angka
            nostruk = Format(Now, "yyMMdd") & nostruk
        Else
            'ga ada data
            nostruk = Format(Now, "yyMMdd") & "001"
        End If
        TxtNoStruk.Text = nostruk
        dtpTgl.Value = Now
    End Sub

    Private Sub TxtCustomerCode_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TxtCustomerCode.TextChanged
        Dim ds As DataSet
        ds = QueryToDataSet("select * from m_customer where " & _
                             "customer_code='" & TxtCustomerCode.Text & "' ")
        If ds.Tables(0).Rows.Count > 0 Then
            TxtCustomerName.Text = ds.Tables(0).Rows(0).Item("customer_name")
        Else
            TxtCustomerName.Text = ""
        End If
    End Sub

    Private Sub TxtItemCode_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TxtItemCode.TextChanged
        Dim ds As DataSet
        ds = QueryToDataSet("select * from m_item where " & _
                             "item_code='" & TxtItemCode.Text & "' ")
        If ds.Tables(0).Rows.Count > 0 Then
            TxtItemName.Text = ds.Tables(0).Rows(0).Item("item_name")
            TxtPrice.Text = ds.Tables(0).Rows(0).Item("price")
        Else
            TxtItemName.Text = ""
            TxtPrice.Text = ""
        End If
    End Sub

    Private Sub BtnCariCustomerCode_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnCariCustomerCode.Click
        Dim ds As DataSet
        ds = QueryToDataSet("select * from m_customer")
        FrmLookup.Dg.DataSource = ds.Tables(0)
        FrmLookup.Text = "customer"
        FrmLookup.ShowDialog()
    End Sub

    Private Sub BtnCariItemCode_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnCariItemCode.Click
        Dim ds As DataSet
        ds = QueryToDataSet("select * from m_item")
        FrmLookup.Dg.DataSource = ds.Tables(0)
        FrmLookup.Text = "item"
        FrmLookup.ShowDialog()
    End Sub

    Private Sub TxtSubtotalDetail_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TxtSubtotalDetail.TextChanged
        
    End Sub

    Private Sub TxtQty_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles TxtQty.KeyDown
        If e.KeyCode = 13 Then
            simpanD()
            isiSubtotal()
            simpanH()
            tampilGrid()
        End If
    End Sub
    Private Sub simpanD()
        Dim ds As DataSet
        ds = QueryToDataSet("select * from t_struk_detail " & _
                            "where struk_no='" & TxtNoStruk.Text & "' and " & _
                            "item_code='" & TxtItemCode.Text & "'")
        If ds.Tables(0).Rows.Count > 0 Then
            ExecuteQuery("update t_struk_detail set " & _
                          "qty='" & TxtQty.Text & "', " & _
                          "subtotal='" & TxtSubtotalDetail.Text & "' " & _
                          "where struk_no='" & TxtNoStruk.Text & "' and " & _
                          "item_code='" & TxtItemCode.Text & "'")
        Else
            ExecuteQuery("insert into t_struk_detail values " & _
                         "('" & TxtNoStruk.Text & "', " & _
                         "'" & TxtItemCode.Text & "', " & _
                         "'" & TxtQty.Text & "', " & _
                         "'" & TxtSubtotalDetail.Text & "' )")
        End If
    End Sub
    Private Sub isiSubtotal()
        Dim subtotalH, ppn, grandtotal, bayar, kembali As Double
        Dim ds As DataSet
        ds = QueryToDataSet("select sum(subtotal) as subtotal " & _
                             "from t_struk_detail " & _
                             "where struk_no='" & TxtNoStruk.Text & "' " & _
                             "group by struk_no")
        If ds.Tables(0).Rows.Count > 0 Then
            subtotalH = ds.Tables(0).Rows(0).Item("subtotal").ToString
        Else
            subtotalH = 0
        End If
        TxtSubtotalHeader.Text = subtotalH

        ppn = subtotalH * 10 / 100
        TxtPpn.Text = ppn
        grandtotal = subtotalH + ppn
        TxtGrandtotal.Text = grandtotal

        ds = QueryToDataSet("select bayar from t_struk " & _
                             "where struk_no='" & TxtNoStruk.Text & "' ")
        If ds.Tables(0).Rows.Count > 0 Then
            bayar = ds.Tables(0).Rows(0).Item("bayar").ToString
        Else
            bayar = 0
        End If
        TxtBayar.Text = bayar
        kembali = bayar - grandtotal
        TxtKembali.Text = kembali
    End Sub
    Private Sub simpanH()
        Dim ds As DataSet
        ds = QueryToDataSet("select * from t_struk " & _
        "where struk_no='" & TxtNoStruk.Text & "' ")
        If ds.Tables(0).Rows.Count > 0 Then
            ExecuteQuery("update t_struk set " & _
            "tgl='" & Format(dtpTgl.Value, "yyyy-MM-dd") & "', " & _
            "customer_code='" & TxtCustomerCode.Text & "', " & _
            "subtotal='" & TxtSubtotalHeader.Text & "', " & _
            "ppn='" & TxtPpn.Text & "', " & _
            "grandtotal='" & TxtGrandtotal.Text & "', " & _
            "bayar='" & TxtBayar.Text & "', " & _
            "kembali='" & TxtKembali.Text & "' " & _
            "where struk_no='" & TxtNoStruk.Text & "' ")
        Else
            ExecuteQuery("insert into t_struk values " & _
            "('" & TxtNoStruk.Text & "', " & _
            " '" & Format(dtpTgl.Value, "yyyy-MM-dd") & "', " & _
            " '" & TxtCustomerCode.Text & "', " & _
            " '" & TxtSubtotalHeader.Text & "', " & _
            " '" & TxtPpn.Text & "', " & _
            " '" & TxtGrandtotal.Text & "', " & _
            " '" & TxtBayar.Text & "', " & _
            " '" & TxtKembali.Text & "' )")
        End If
    End Sub
    Private Sub tampilGrid()
        Dim ds As DataSet
        ds = QueryToDataSet("select sd.item_code, i.item_name, " & _
                            "i.price, sd.qty, sd.subtotal " & _
                            "from t_struk_detail sd " & _
                            "left join m_item i on " & _
                            "sd.item_code=i.item_code " & _
                            "where sd.struk_no='" & TxtNoStruk.Text & "'")
        DgPenjualan.DataSource = ds.Tables(0)

    End Sub

    Private Sub TxtQty_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TxtQty.TextChanged
        Dim subtotalD, price, qty As Double

        If TxtPrice.Text = "" Then
            price = 0
        Else
            price = CDbl(TxtPrice.Text)
        End If

        If TxtQty.Text = "" Then
            qty = 0
        Else
            qty = CDbl(TxtQty.Text)
        End If

        subtotalD = price * qty
        TxtSubtotalDetail.Text = subtotalD
    End Sub

    Private Sub BtnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnClose.Click
        Close()
    End Sub

    Private Sub TxtBayar_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TxtBayar.TextChanged
        Dim grandTotal, kembali, bayar As Double
        If TxtGrandtotal.Text = "" Then
            grandTotal = 0
        Else
            grandTotal = CDbl(TxtGrandtotal.Text)
        End If
        If TxtKembali.Text = "" Then
            kembali = 0
        Else
            kembali = CDbl(TxtKembali.Text)
        End If
        If TxtBayar.Text = "" Then
            bayar = 0
        Else
            bayar = CDbl(TxtBayar.Text)
        End If
        kembali = bayar - grandTotal
        TxtKembali.Text = kembali
    End Sub

    Private Sub TxtKembali_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TxtKembali.TextChanged

    End Sub

    Private Sub TxtSubtotalHeader_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TxtSubtotalHeader.TextChanged

    End Sub

    Private Sub BtnSimpan_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnSimpan.Click
        simpanH()
        clearALL()
    End Sub
    Private Sub clearALL()
        TxtNoStruk.Text = ""
        TxtItemCode.Text = ""
        TxtQty.Text = "0"
        TxtSubtotalDetail.Text = "0"
    End Sub

    Private Sub TxtNoStruk_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TxtNoStruk.TextChanged
        Dim ds As DataSet
        ds = QueryToDataSet("select * from t_struk where " & _
                             "struk_no='" & TxtNoStruk.Text & "' ")
        If ds.Tables(0).Rows.Count > 0 Then
            dtpTgl.Value = ds.Tables(0).Rows(0).Item("tgl")
            TxtCustomerCode.Text = ds.Tables(0).Rows(0).Item("customer_code")

        End If
        tampilGrid()

    End Sub

    Private Sub BtnHapus_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnHapus.Click
        If MsgBox("Anda Yakin Ingin Delete?", MsgBoxStyle.YesNo + MsgBoxStyle.Question, _
                  "confirm Delete") = MsgBoxResult.Yes Then

            ExecuteQuery("delete from t_struk " & _
                         "where struk_no='" & TxtNoStruk.Text & "' ")
            ExecuteQuery("delete from t_struk_detail " & _
                         "where struk_no='" & TxtNoStruk.Text & "' ")
            clearALL()
        End If
    End Sub

    Private Sub BtnCetak_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnCetak.Click
        ExecuteQuery("drop table tmp_struk")
        ExecuteQuery("CREATE TABLE tmp_struk as " & _
        "SELECT sh.struk_no, sh.tgl, " & _
        "sh.customer_code, c.customer_name, " & _
        "sd.item_code, i.item_name, i.price, " & _
        "sd.qty, sd.subtotal AS subtotal_detail, " & _
        "sh.subtotal as subtotal_header, " & _
        "sh.ppn, sh.bayar, sh.grandtotal, " & _
        "sh.kembali FROM t_struk sh " & _
        "LEFT JOIN t_struk_detail sd " & _
        "ON sh.struk_no=sd.struk_no " & _
        "LEFT JOIN m_customer c " & _
        "ON sh.customer_code=c.customer_code " & _
        "LEFT JOIN m_item i " & _
        "ON sd.item_code=i.item_code " & _
        "WHERE sh.struk_no='" & TxtNoStruk.Text & "'")

        Dim ds As DataSet
        ds = QueryToDataSet("select * from tmp_struk")
        Dim Rpt As New RptStruk
        FrmReport.CrystalReportViewer1.ReportSource = Rpt
        FrmReport.CrystalReportViewer1.Refresh()
        FrmReport.ShowDialog()

    End Sub

    Private Sub BtnCari_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnCari.Click
        Dim s As String
        s = InputBox("Masukan No.Struk", "Cari No.Struk", "")

        Dim ds As New DataSet 'Table jadi
        ds = QueryToDataSet("select * from t_struk where struk_no like '%" & s & "%'")
        FrmLookup.Dg.DataSource = ds.Tables(0)
        FrmLookup.Text = "cari struk"
        FrmLookup.ShowDialog()
    End Sub
End Class