﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmStok
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.LblStok = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.DtPeriode = New System.Windows.Forms.DateTimePicker
        Me.BtnCetak = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'LblStok
        '
        Me.LblStok.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.LblStok.Dock = System.Windows.Forms.DockStyle.Top
        Me.LblStok.Font = New System.Drawing.Font("Modern No. 20", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblStok.Location = New System.Drawing.Point(0, 0)
        Me.LblStok.Name = "LblStok"
        Me.LblStok.Size = New System.Drawing.Size(373, 48)
        Me.LblStok.TabIndex = 1
        Me.LblStok.Text = "Laporan Stok"
        Me.LblStok.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(19, 91)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(43, 13)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Periode"
        '
        'DtPeriode
        '
        Me.DtPeriode.CustomFormat = "dddd, dd MM yyyy"
        Me.DtPeriode.Location = New System.Drawing.Point(97, 91)
        Me.DtPeriode.Name = "DtPeriode"
        Me.DtPeriode.Size = New System.Drawing.Size(240, 20)
        Me.DtPeriode.TabIndex = 3
        '
        'BtnCetak
        '
        Me.BtnCetak.Location = New System.Drawing.Point(246, 118)
        Me.BtnCetak.Name = "BtnCetak"
        Me.BtnCetak.Size = New System.Drawing.Size(75, 23)
        Me.BtnCetak.TabIndex = 4
        Me.BtnCetak.Text = "Cetak"
        Me.BtnCetak.UseVisualStyleBackColor = True
        '
        'FrmStok
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(373, 172)
        Me.Controls.Add(Me.BtnCetak)
        Me.Controls.Add(Me.DtPeriode)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.LblStok)
        Me.Name = "FrmStok"
        Me.Text = "FrmStok"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents LblStok As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents DtPeriode As System.Windows.Forms.DateTimePicker
    Friend WithEvents BtnCetak As System.Windows.Forms.Button
End Class
