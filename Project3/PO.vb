﻿Imports System.Data.Odbc
Public Class Purchase_order
    Private Sub simpanPO()
        Dim ds As DataSet

        ds = QueryToDataSet("select * from t_po where " & _
                            "po_no='" & TxtNoPO.Text & "' and " & _
                            "tgl='" & Format(DtpTgl.Value, "yyyy-MM-dd") & "' ")
        If ds.Tables(0).Rows.Count > 0 Then
            'ada DATA , update
            ExecuteQuery("update t_po set " & _
            "pr_no='" & TxtNoPR.Text & "', " & _
            "tgl='" & Format(DtpTgl.Value, "yyyy-MM-dd") & "', " & _
            "vendor_code='" & TxtVendor.Text & "', " & _
            "subtotal='" & TxtSubtotal.Text & "', " & _
            "ppn='" & TxtPPN.Text & "', " & _
            "grandtotal='" & TxtGrandtotal.Text & "' " & _
            "where " & _
            "po_no='" & TxtNoPO.Text & "' and " & _
            "tgl='" & Format(DtpTgl.Value, "yyyy-MM-dd") & "' ")
        Else
            ExecuteQuery("insert into t_po values ('" & TxtNoPO.Text & "', " & _
                        "'" & TxtNoPR.Text & "', " & _
                        "'" & Format(DtpTgl.Value, "yyyy-MM-dd") & "', " & _
                        "'" & TxtVendor.Text & "', " & _
                        "'" & TxtSubtotal.Text & "', " & _
                        "'" & TxtPPN.Text & "', " & _
                        "'" & TxtGrandtotal.Text & "')")
        End If
    End Sub

    Private Sub BtnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnSave.Click
        simpanPO()
        tampil_purchase_order()
    End Sub
    Private Sub tampil_Dg_PO()
        Dim ds As DataSet
        ds = QueryToDataSet("select id.item_code,it.item_name, " & _
                            "it.price,id.qty,id.subtotal " & _
                            "from t_po_detail id " & _
                            "left join m_item it on id.item_code=it.item_code " & _
                            "where id.po_no='" & TxtNoPO.Text & "'")
        DgPurchaseOrder.DataSource = ds.Tables(0)
    End Sub
    Private Sub tampil_purchase_order()
        Dim ds As DataSet
        ds = QueryToDataSet("select * from t_po where po_no='" & TxtNoPO.Text & "' ")
        DgPurchaseOrder.DataSource = ds.Tables(0)
    End Sub

    Private Sub Txtsubtotal_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TxtSubtotal.TextChanged
        Dim subtotalH, ppn, grandtotal As Double
        If TxtSubtotal.Text = "" Then
            subtotalH = 0
        Else
            subtotalH = CDbl(TxtSubtotal.Text)
        End If
        If TxtPPN.Text = "" Then
            ppn = 0
        Else
            ppn = CDbl(TxtPPN.Text)
        End If
        If TxtGrandtotal.Text = "" Then
            grandtotal = 0
        Else
            grandtotal = CDbl(TxtGrandtotal.Text)
        End If
        TxtSubtotal.Text = subtotalH

        ppn = subtotalH * 10 / 100
        TxtPPN.Text = ppn

        grandtotal = subtotalH + ppn
        TxtGrandtotal.Text = grandtotal
    End Sub

    Private Sub Txtitemcode_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TxtItemCode.TextChanged
        Dim ds As DataSet
        ds = QueryToDataSet("select * from m_item where " & _
                            "item_code='" & TxtItemCode.Text & "' ")
        If ds.Tables(0).Rows.Count = 0 Then
            'ga ada data
            TxtItemName.Clear()
            TxtPrice.Clear()
            'Txtqty.Clear()
            'Txtsubtotal.Clear()
            'txtNamaBarang.Text = "" 'untuk combobox
        Else
            'ada data
            TxtItemName.Text = ds.Tables(0).Rows(0).Item("item_name").ToString
            TxtPrice.Text = ds.Tables(0).Rows(0).Item("price").ToString
        End If
        tampil_purchase_order()
    End Sub

    Private Sub TxtQty_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles TxtQty.KeyDown
        If e.KeyCode = 13 Then
            simpanPurchaseD()
            '    TampilSubtotalHeader()
            '    SimpanInvoiceH()
            tampil_Dg_PO()
        End If
    End Sub

    Private Sub simpanPurchaseD()

        Dim ds As DataSet
        ds = QueryToDataSet("select * from t_po_detail where " & _
            "po_no='" & TxtNoPO.Text & "' and " & _
            "item_code='" & TxtItemCode.Text & "' ")



        If ds.Tables(0).Rows.Count = 0 Then
            'primary key blm ada, insert
            ds = QueryToDataSet("insert into t_po_detail values " & _
            "('" & TxtNoPO.Text & "', " & _
            " '" & TxtItemCode.Text & "', " & _
            " '" & TxtQty.Text & "', " & _
            " '" & TxtSubtotal.Text & "')")
        Else
            'primary key ada, update
            ds = QueryToDataSet("update t_po_detail set " & _
            "qty='" & TxtQty.Text & "', " & _
            "subtotal='" & TxtSubtotal.Text & "' " & _
            "where " & _
            "po_no='" & TxtNoPO.Text & "' and " & _
            "item_code='" & TxtItemCode.Text & "'")
        End If
        tampil_purchase_order()
    End Sub

    Private Sub Txtqty_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TxtQty.TextChanged

        Dim price, qty, subtotal
        If TxtPrice.Text = "" Then
            price = 0
        Else
            price = CDbl(TxtPrice.Text)

        End If
        If TxtQty.Text = "" Then
            qty = 0
        Else
            qty = CDbl(TxtQty.Text)
        End If
        subtotal = price * qty
        TxtSubtotal.Text = subtotal
        tampil_purchase_order()
    End Sub

    Private Sub LblPurchaseOrder_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LblPurchaseOrder.Click

    End Sub

    Private Sub DgPurchaseOrder_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DgPurchaseOrder.CellContentClick

    End Sub

    Private Sub Purchase_order_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        tampil_Dg_PO()
    End Sub

    Private Sub BtnNoPO_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnNoPO.Click
        Dim ds As DataSet
        ds = QueryToDataSet("select * from t_po")
        FrmLookup.Dg.DataSource = ds.Tables(0)
        FrmLookup.Text = "po"
        FrmLookup.ShowDialog()
    End Sub

    Private Sub TxtNoPO_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TxtNoPO.TextChanged
        tampil_purchase_order()
    End Sub

    Private Sub BtnNoPR_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnNoPR.Click
        Dim ds As DataSet
        ds = QueryToDataSet("select * from t_pr")
        FrmLookup.Dg.DataSource = ds.Tables(0)
        FrmLookup.Text = "pr"
        FrmLookup.ShowDialog()
    End Sub

    Private Sub BtnVendor_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnVendor.Click
        Dim ds As DataSet
        ds = QueryToDataSet("select * from m_vendor")
        FrmLookup.Dg.DataSource = ds.Tables(0)
        FrmLookup.Text = "po_detail"
        FrmLookup.ShowDialog()
    End Sub

    Private Sub BtnItemCode_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnItemCode.Click
        Dim ds As DataSet
        ds = QueryToDataSet("select * from m_item")
        FrmLookup.Dg.DataSource = ds.Tables(0)
        FrmLookup.Text = "p_item"
        FrmLookup.ShowDialog()
    End Sub

    Private Sub BtnDeleteTransaksi_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnDeleteTransaksi.Click
        If MsgBox("Anda Yakin Ingin Dihapus?", _
                   MsgBoxStyle.YesNo + MsgBoxStyle.Question, _
                   "Konfirmasi Delete") = MsgBoxResult.Yes Then
            Dim ds As New DataSet
            ds = QueryToDataSet("delete from t_po_detail where " & _
                            "po_no='" & TxtNoPO.Text & "' and item_code='" & TxtItemCode.Text & "'")
        End If
    End Sub

    Private Sub DtpTgl_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DtpTgl.ValueChanged

    End Sub

    Private Sub BtnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnNew.Click
        TxtNoPO.Clear()
        DtpTgl.Value = Now
        TxtNoPR.Clear()
        TxtPPN.Clear()
        TxtPrice.Clear()
        TxtItemCode.Clear()
        TxtItemName.Clear()
        TxtVendor.Clear()
        TxtQty.Clear()
        TxtSubtotal.Clear()
        TxtSubtotalH.Clear()
        TxtGrandtotal.Clear()
        TxtNoPO.Focus()
    End Sub
End Class
