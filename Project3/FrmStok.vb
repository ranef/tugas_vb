﻿Public Class FrmStok

    Private Sub Label1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label1.Click

    End Sub

    Private Sub BtnCetak_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnCetak.Click
        Dim bln2, thn2, bln1, thn1 As Integer
        bln2 = Month(DtPeriode.Value)
        thn2 = Year(DtPeriode.Value)

        bln1 = Month(DtPeriode.Value.AddMonths(-1))
        thn1 = Year(DtPeriode.Value.AddMonths(-1))

        ExecuteQuery("delete from t_stok where " & _
                      "month(periode)='" & bln2 & "'and " & _
                      "year(periode)='" & thn2 & "' ")

        ExecuteQuery("insert into t_stok(periode,item_code) " & _
                    "select '" & Format(DtPeriode.Value, "yyyy-MM-dd") & "' as periode, item_code from m_item ")

        ExecuteQuery("update t_stok a, " & _
                    "(select rd.item_code, sum(rd.qty)as masuk " & _
                    "from t_ri_detail rd " & _
                    "left join t_ri rh on rd.ri_no=rh.ri_no " & _
                    "where(Month(rh.tgl_ri) = '" & bln1 & "' And Year(rh.tgl_ri) = '" & thn1 & "') " & _
                    "group by rd.item_code " & _
                    ") b " & _
                    "set a.masuk = b.masuk " & _
                    "where(a.item_code = b.item_code) " & _
                    "and month(periode)='" & bln2 & "' and year(periode)='" & thn2 & "' ")

        ExecuteQuery("update t_stok a, " & _
                    "(select id.item_code, sum(id.qty)as keluar " & _
                    "from t_inv_detail id " & _
                    "left join t_inv ih on id.inv_no=ih.inv_no " & _
                    "where(Month(ih.tgl_inv) ='" & bln1 & "' And Year(ih.tgl_inv) ='" & thn1 & "') " & _
                    "group by id.item_code " & _
                    ") b " & _
                    "set a.keluar = b.keluar " & _
                    "where(a.item_code = b.item_code) " & _
                    "and month(periode)='" & bln2 & "' and year(periode)='" & thn2 & "' ")
        ExecuteQuery("update t_stok set sisa=last+masuk-keluar " & _
                    "where month(periode)='" & bln2 & "' and year(periode)='" & thn2 & "' ")

        ExecuteQuery("update t_stok a, " & _
                    "(select item_code, sisa as last " & _
                    "from t_stok where month(periode)='" & bln1 & "' and year(periode)='" & thn1 & "'" & _
                    ")b " & _
                    "set a.last = coalesce(b.last, 0) " & _
                    "where(a.item_code = b.item_code) " & _
                    "and month(periode)='" & bln2 & "' and year(periode)='" & thn2 & "'")


        Dim ds As DataSet
        ds = QueryToDataSet("select * from t_stok")
        Dim Rpt As New RptStok
        FrmReport.CrystalReportViewer1.ReportSource = Rpt
        FrmReport.CrystalReportViewer1.Refresh()
        FrmReport.ShowDialog()

    End Sub
End Class