﻿Imports System.Data.Odbc
Public Class Frm_item
    Private Sub Btn_new_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btn_new.Click
        Txt_item_code.Clear()
        Txt_item_name.Clear()
        Txt_price.Clear()
        Txt_item_code.Focus()
    End Sub

    Private Sub dg_item_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dg_item.CellContentClick
        Txt_item_code.Text = dg_item.Item(1, dg_item.CurrentRow.Index).Value
        Txt_item_name.Text = dg_item.Item(2, dg_item.CurrentRow.Index).Value
        Txt_price.Text = dg_item.Item(3, dg_item.CurrentRow.Index).Value
    End Sub

    Private Sub Btn_item_code_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btn_item_code.Click
        Dim s As String
        s = InputBox("Masukan Itemcode", "Cari Itemnama", "")

        Dim ds As New DataSet 'table jadi
        ds = QueryToDataset("select * from m_item where item_code like'%" & s & "%'")
        FrmLookup.Dg.DataSource = ds.Tables(0)
        FrmLookup.Text = "m_item"
        FrmLookup.ShowDialog()
    End Sub

    Private Sub tampilitem()
        Dim ds As DataSet
        ds = QueryToDataset("select * from m_item")
        dg_item.DataSource = ds.Tables(0)
    End Sub

    Private Sub Btn_save_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btn_save.Click
        Dim ds As DataSet

        ds = QueryToDataset("select * from m_item where " & _
                            "item_code='" & Txt_item_code.Text & "'")

        If ds.Tables(0).Rows.Count > 0 Then
            'ada DATA update
            ExecuteQuery("update m_item where item_code='" & Txt_item_code.Text & "' ")
        Else
            'Tidak ada DATA insert
            ExecuteQuery("insert into m_item values ('" & Txt_item_code.Text & "','" & Txt_item_name.Text & "', " & _
                                                        " '" & Txt_price.Text & "') ")
        End If
        tampilitem()
    End Sub

    Private Sub Btn_delete_transaksi_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btn_delete_transaksi.Click
        ExecuteQuery("delete from m_item where item_code='" & Txt_item_code.Text & "' ")
        tampilitem()
    End Sub

    Private Sub Txt_item_code_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Txt_item_code.TextChanged

    End Sub

    Private Sub Btn_Save_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btn_Save.Click
        Dim ds As DataSet

        ds = QueryToDataset("select * from m_item where " & _
                            "item_code='" & Txt_item_code.Text & "' ")
        If ds.Tables(0).Rows.Count > 0 Then
            'ada DATA , update
            ExecuteQuery("update m_item set " & _
            "item_name='" & Txt_item_name.Text & "', " & _
            "price='" & Txt_price.Text & "' " & _
            "where item_code='" & Txt_item_code.Text & "' ")
        Else
            'tidak ada DATA , insert
            ExecuteQuery("insert into m_item values ( '" & Txt_item_code.Text & "', " & _
                         " '" & Txt_item_name.Text & "', " & _
                         " '" & Txt_price.Text & "') ")
        End If
        tampilitem()
    End Sub

    Private Sub Frm_item_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        tampilitem()
    End Sub
End Class