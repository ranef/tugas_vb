﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmVendor
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.LblVendor = New System.Windows.Forms.Label
        Me.LblVendorCode = New System.Windows.Forms.Label
        Me.LblVendorName = New System.Windows.Forms.Label
        Me.TxtBoxVendorCode = New System.Windows.Forms.TextBox
        Me.TxtBoxVendorName = New System.Windows.Forms.TextBox
        Me.DgVendor = New System.Windows.Forms.DataGridView
        Me.BtnNew = New System.Windows.Forms.Button
        Me.BtnSave = New System.Windows.Forms.Button
        Me.BtnDeleteTransaksi = New System.Windows.Forms.Button
        Me.BtnPrint = New System.Windows.Forms.Button
        Me.BtnCariVendorCode = New System.Windows.Forms.Button
        Me.reportDocument1 = New CrystalDecisions.CrystalReports.Engine.ReportDocument
        CType(Me.DgVendor, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'LblVendor
        '
        Me.LblVendor.AutoSize = True
        Me.LblVendor.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblVendor.Location = New System.Drawing.Point(268, 9)
        Me.LblVendor.Name = "LblVendor"
        Me.LblVendor.Size = New System.Drawing.Size(58, 16)
        Me.LblVendor.TabIndex = 0
        Me.LblVendor.Text = "Vendor"
        '
        'LblVendorCode
        '
        Me.LblVendorCode.AutoSize = True
        Me.LblVendorCode.Location = New System.Drawing.Point(29, 54)
        Me.LblVendorCode.Name = "LblVendorCode"
        Me.LblVendorCode.Size = New System.Drawing.Size(69, 13)
        Me.LblVendorCode.TabIndex = 1
        Me.LblVendorCode.Text = "Vendor Code"
        '
        'LblVendorName
        '
        Me.LblVendorName.AutoSize = True
        Me.LblVendorName.Location = New System.Drawing.Point(29, 87)
        Me.LblVendorName.Name = "LblVendorName"
        Me.LblVendorName.Size = New System.Drawing.Size(72, 13)
        Me.LblVendorName.TabIndex = 2
        Me.LblVendorName.Text = "Vendor Name"
        '
        'TxtBoxVendorCode
        '
        Me.TxtBoxVendorCode.Location = New System.Drawing.Point(130, 54)
        Me.TxtBoxVendorCode.Name = "TxtBoxVendorCode"
        Me.TxtBoxVendorCode.Size = New System.Drawing.Size(196, 20)
        Me.TxtBoxVendorCode.TabIndex = 3
        '
        'TxtBoxVendorName
        '
        Me.TxtBoxVendorName.Location = New System.Drawing.Point(130, 80)
        Me.TxtBoxVendorName.Name = "TxtBoxVendorName"
        Me.TxtBoxVendorName.Size = New System.Drawing.Size(196, 20)
        Me.TxtBoxVendorName.TabIndex = 4
        '
        'DgVendor
        '
        Me.DgVendor.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DgVendor.Location = New System.Drawing.Point(32, 127)
        Me.DgVendor.Name = "DgVendor"
        Me.DgVendor.Size = New System.Drawing.Size(495, 150)
        Me.DgVendor.TabIndex = 5
        '
        'BtnNew
        '
        Me.BtnNew.Location = New System.Drawing.Point(32, 284)
        Me.BtnNew.Name = "BtnNew"
        Me.BtnNew.Size = New System.Drawing.Size(132, 23)
        Me.BtnNew.TabIndex = 6
        Me.BtnNew.Text = "New"
        Me.BtnNew.UseVisualStyleBackColor = True
        '
        'BtnSave
        '
        Me.BtnSave.Location = New System.Drawing.Point(170, 284)
        Me.BtnSave.Name = "BtnSave"
        Me.BtnSave.Size = New System.Drawing.Size(124, 23)
        Me.BtnSave.TabIndex = 7
        Me.BtnSave.Text = "Save"
        Me.BtnSave.UseVisualStyleBackColor = True
        '
        'BtnDeleteTransaksi
        '
        Me.BtnDeleteTransaksi.Location = New System.Drawing.Point(170, 313)
        Me.BtnDeleteTransaksi.Name = "BtnDeleteTransaksi"
        Me.BtnDeleteTransaksi.Size = New System.Drawing.Size(124, 23)
        Me.BtnDeleteTransaksi.TabIndex = 9
        Me.BtnDeleteTransaksi.Text = "Delete Transaksi"
        Me.BtnDeleteTransaksi.UseVisualStyleBackColor = True
        '
        'BtnPrint
        '
        Me.BtnPrint.Location = New System.Drawing.Point(32, 313)
        Me.BtnPrint.Name = "BtnPrint"
        Me.BtnPrint.Size = New System.Drawing.Size(132, 23)
        Me.BtnPrint.TabIndex = 8
        Me.BtnPrint.Text = "Print"
        Me.BtnPrint.UseVisualStyleBackColor = True
        '
        'BtnCariVendorCode
        '
        Me.BtnCariVendorCode.Location = New System.Drawing.Point(330, 54)
        Me.BtnCariVendorCode.Name = "BtnCariVendorCode"
        Me.BtnCariVendorCode.Size = New System.Drawing.Size(26, 20)
        Me.BtnCariVendorCode.TabIndex = 31
        Me.BtnCariVendorCode.Text = "..."
        Me.BtnCariVendorCode.UseVisualStyleBackColor = True
        '
        'reportDocument1
        '
        '
        'FrmVendor
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(555, 366)
        Me.Controls.Add(Me.BtnCariVendorCode)
        Me.Controls.Add(Me.BtnDeleteTransaksi)
        Me.Controls.Add(Me.BtnPrint)
        Me.Controls.Add(Me.BtnSave)
        Me.Controls.Add(Me.BtnNew)
        Me.Controls.Add(Me.DgVendor)
        Me.Controls.Add(Me.TxtBoxVendorName)
        Me.Controls.Add(Me.TxtBoxVendorCode)
        Me.Controls.Add(Me.LblVendorName)
        Me.Controls.Add(Me.LblVendorCode)
        Me.Controls.Add(Me.LblVendor)
        Me.Name = "FrmVendor"
        Me.Text = "FrmVendor"
        CType(Me.DgVendor, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents LblVendor As System.Windows.Forms.Label
    Friend WithEvents LblVendorCode As System.Windows.Forms.Label
    Friend WithEvents LblVendorName As System.Windows.Forms.Label
    Friend WithEvents TxtBoxVendorCode As System.Windows.Forms.TextBox
    Friend WithEvents TxtBoxVendorName As System.Windows.Forms.TextBox
    Friend WithEvents DgVendor As System.Windows.Forms.DataGridView
    Friend WithEvents BtnNew As System.Windows.Forms.Button
    Friend WithEvents BtnSave As System.Windows.Forms.Button
    Friend WithEvents BtnDeleteTransaksi As System.Windows.Forms.Button
    Friend WithEvents BtnPrint As System.Windows.Forms.Button
    Friend WithEvents BtnCariVendorCode As System.Windows.Forms.Button
    Friend WithEvents reportDocument1 As CrystalDecisions.CrystalReports.Engine.ReportDocument

End Class
