﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Frm_customer
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Btn_delete_transaksi = New System.Windows.Forms.Button
        Me.Btn_save = New System.Windows.Forms.Button
        Me.Btn_print = New System.Windows.Forms.Button
        Me.Btn_new = New System.Windows.Forms.Button
        Me.dg_customer = New System.Windows.Forms.DataGridView
        Me.Txt_customer_code = New System.Windows.Forms.TextBox
        Me.Lbl_customer_name = New System.Windows.Forms.Label
        Me.Lbl_customer_code = New System.Windows.Forms.Label
        Me.LblCustomer = New System.Windows.Forms.Label
        Me.Btn_customer_code = New System.Windows.Forms.Button
        Me.Txt_customer_name = New System.Windows.Forms.TextBox
        CType(Me.dg_customer, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Btn_delete_transaksi
        '
        Me.Btn_delete_transaksi.Location = New System.Drawing.Point(167, 392)
        Me.Btn_delete_transaksi.Name = "Btn_delete_transaksi"
        Me.Btn_delete_transaksi.Size = New System.Drawing.Size(103, 23)
        Me.Btn_delete_transaksi.TabIndex = 19
        Me.Btn_delete_transaksi.Text = "Delete Transaksi"
        Me.Btn_delete_transaksi.UseVisualStyleBackColor = True
        '
        'Btn_save
        '
        Me.Btn_save.Location = New System.Drawing.Point(167, 358)
        Me.Btn_save.Name = "Btn_save"
        Me.Btn_save.Size = New System.Drawing.Size(103, 23)
        Me.Btn_save.TabIndex = 18
        Me.Btn_save.Text = "Save"
        Me.Btn_save.UseVisualStyleBackColor = True
        '
        'Btn_print
        '
        Me.Btn_print.Location = New System.Drawing.Point(50, 392)
        Me.Btn_print.Name = "Btn_print"
        Me.Btn_print.Size = New System.Drawing.Size(98, 23)
        Me.Btn_print.TabIndex = 17
        Me.Btn_print.Text = "Print"
        Me.Btn_print.UseVisualStyleBackColor = True
        '
        'Btn_new
        '
        Me.Btn_new.Location = New System.Drawing.Point(50, 358)
        Me.Btn_new.Name = "Btn_new"
        Me.Btn_new.Size = New System.Drawing.Size(98, 23)
        Me.Btn_new.TabIndex = 16
        Me.Btn_new.Text = "New"
        Me.Btn_new.UseVisualStyleBackColor = True
        '
        'dg_customer
        '
        Me.dg_customer.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dg_customer.Location = New System.Drawing.Point(50, 122)
        Me.dg_customer.Name = "dg_customer"
        Me.dg_customer.Size = New System.Drawing.Size(628, 224)
        Me.dg_customer.TabIndex = 15
        '
        'Txt_customer_code
        '
        Me.Txt_customer_code.Location = New System.Drawing.Point(174, 52)
        Me.Txt_customer_code.Name = "Txt_customer_code"
        Me.Txt_customer_code.Size = New System.Drawing.Size(157, 20)
        Me.Txt_customer_code.TabIndex = 13
        '
        'Lbl_customer_name
        '
        Me.Lbl_customer_name.AutoSize = True
        Me.Lbl_customer_name.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Lbl_customer_name.Location = New System.Drawing.Point(47, 85)
        Me.Lbl_customer_name.Name = "Lbl_customer_name"
        Me.Lbl_customer_name.Size = New System.Drawing.Size(118, 18)
        Me.Lbl_customer_name.TabIndex = 12
        Me.Lbl_customer_name.Text = "Customer Name"
        '
        'Lbl_customer_code
        '
        Me.Lbl_customer_code.AutoSize = True
        Me.Lbl_customer_code.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Lbl_customer_code.Location = New System.Drawing.Point(47, 52)
        Me.Lbl_customer_code.Name = "Lbl_customer_code"
        Me.Lbl_customer_code.Size = New System.Drawing.Size(114, 18)
        Me.Lbl_customer_code.TabIndex = 11
        Me.Lbl_customer_code.Text = "Customer Code"
        '
        'LblCustomer
        '
        Me.LblCustomer.AutoSize = True
        Me.LblCustomer.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblCustomer.Location = New System.Drawing.Point(273, 10)
        Me.LblCustomer.Name = "LblCustomer"
        Me.LblCustomer.Size = New System.Drawing.Size(112, 25)
        Me.LblCustomer.TabIndex = 10
        Me.LblCustomer.Text = "Customer"
        '
        'Btn_customer_code
        '
        Me.Btn_customer_code.Location = New System.Drawing.Point(337, 50)
        Me.Btn_customer_code.Name = "Btn_customer_code"
        Me.Btn_customer_code.Size = New System.Drawing.Size(48, 23)
        Me.Btn_customer_code.TabIndex = 23
        Me.Btn_customer_code.Text = "..."
        Me.Btn_customer_code.UseVisualStyleBackColor = True
        '
        'Txt_customer_name
        '
        Me.Txt_customer_name.Location = New System.Drawing.Point(174, 86)
        Me.Txt_customer_name.Name = "Txt_customer_name"
        Me.Txt_customer_name.Size = New System.Drawing.Size(211, 20)
        Me.Txt_customer_name.TabIndex = 24
        '
        'Frm_customer
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(709, 424)
        Me.Controls.Add(Me.Txt_customer_name)
        Me.Controls.Add(Me.Btn_customer_code)
        Me.Controls.Add(Me.Btn_delete_transaksi)
        Me.Controls.Add(Me.Btn_save)
        Me.Controls.Add(Me.Btn_print)
        Me.Controls.Add(Me.Btn_new)
        Me.Controls.Add(Me.dg_customer)
        Me.Controls.Add(Me.Txt_customer_code)
        Me.Controls.Add(Me.Lbl_customer_name)
        Me.Controls.Add(Me.Lbl_customer_code)
        Me.Controls.Add(Me.LblCustomer)
        Me.Name = "Frm_customer"
        Me.Text = "Form Customer"
        CType(Me.dg_customer, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Btn_delete_transaksi As System.Windows.Forms.Button
    Friend WithEvents Btn_save As System.Windows.Forms.Button
    Friend WithEvents Btn_print As System.Windows.Forms.Button
    Friend WithEvents Btn_new As System.Windows.Forms.Button
    Friend WithEvents dg_customer As System.Windows.Forms.DataGridView
    Friend WithEvents Txt_customer_code As System.Windows.Forms.TextBox
    Friend WithEvents Lbl_customer_name As System.Windows.Forms.Label
    Friend WithEvents Lbl_customer_code As System.Windows.Forms.Label
    Friend WithEvents LblCustomer As System.Windows.Forms.Label
    Friend WithEvents Btn_customer_code As System.Windows.Forms.Button
    Friend WithEvents Txt_customer_name As System.Windows.Forms.TextBox
End Class
