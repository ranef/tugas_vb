﻿Imports System.Data.Odbc
Public Class FrmVendor
    Private Sub tampilGridVendor()
        Dim ds As DataSet
        ds = QueryToDataSet("select * from m_vendor")
        DgVendor.DataSource = ds.Tables(0)
    End Sub
    Private Sub tampilVendorCode()
        Dim vKoneksi As OdbcConnection
        vKoneksi = New OdbcConnection("DSN=DSNPROJECT2")
        vKoneksi.Open()

        Dim vCMD As New OdbcCommand
        Dim sda As New OdbcDataAdapter
        Dim ds As New DataSet

        vCMD = vKoneksi.CreateCommand
        vCMD.CommandText = "select * " & _
        "from m_vendor " & _
        "where vendor_code='" & TxtBoxVendorCode.Text & "' "

        vCMD.CommandTimeout = 300
        sda.SelectCommand = vCMD
        sda.Fill(ds)

        If ds.Tables(0).Rows.Count = 0 Then
            TxtBoxVendorCode.Text = ""

        Else
            TxtBoxVendorCode.Text = ds.Tables(0).Rows(0).Item("vendor_code").ToString
        End If
    End Sub
    Private Sub Label1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LblVendorCode.Click

    End Sub

    Private Sub TxtBoxVendorCode_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TxtBoxVendorCode.TextChanged

    End Sub

    Private Sub FrmVendor_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        tampilGridVendor()
    End Sub

    Private Sub BtnCariVendorCode_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnCariVendorCode.Click
        Dim s As String
        s = InputBox("Masukan Nama Vendor", "Cari Vendor", "")
        Dim vKoneksi As OdbcConnection
        vKoneksi = New OdbcConnection("DSN=DSNPROJECT2")
        vKoneksi.Open()

        Dim vCMD As New OdbcCommand
        Dim sda As New OdbcDataAdapter
        Dim ds As New DataSet

        vCMD = vKoneksi.CreateCommand
        vCMD.CommandText = "select * from m_vendor"

        vCMD.CommandTimeout = 300
        sda.SelectCommand = vCMD
        sda.Fill(ds)

        'hubungkan dataset dengan datagirdview
        FrmLookup.Dg.DataSource = ds.Tables(0)
        FrmLookup.Text = "vendor"
        FrmLookup.ShowDialog()
    End Sub

    Private Sub BtnDeleteTransaksi_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnDeleteTransaksi.Click
        ExecuteQuery("delete from m_vendor where " & _
                  "vendor_code='" & TxtBoxVendorCode.Text & "' ")
        tampilGridVendor()
    End Sub

    Private Sub BtnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnSave.Click
        Dim ds As DataSet
        ds = QueryToDataSet("select * from m_vendor where " & _
                            "vendor_code='" & TxtBoxVendorCode.Text & "' ")

        If ds.Tables(0).Rows.Count > 0 Then
            'ada DATA Update'
            ExecuteQuery("update m_vendor set " & _
                         "vendor_name='" & TxtBoxVendorName.Text & "' " & _
                         "where vendor_code='" & TxtBoxVendorCode.Text & "' ")
        Else
            'Tidak ada DATA Insert'
            ExecuteQuery("insert into m_vendor values('" & TxtBoxVendorCode.Text & "','" & TxtBoxVendorName.Text & "')")
        End If
        tampilVendorCode()
        TxtBoxVendorCode.Clear()
        TxtBoxVendorName.Clear()
        tampilGridVendor()
    End Sub

    Private Sub DgVendor_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DgVendor.CellContentClick
        TxtBoxVendorCode.Text = DgVendor.Item(0, DgVendor.CurrentRow.Index).Value
        TxtBoxVendorName.Text = DgVendor.Item(1, DgVendor.CurrentRow.Index).Value
    End Sub

    Private Sub BtnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnNew.Click
        TxtBoxVendorCode.Clear()
        TxtBoxVendorName.Clear()
    End Sub

    Private Sub TxtBoxVendorName_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TxtBoxVendorName.TextChanged

    End Sub

    Private Sub reportDocument1_InitReport(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles reportDocument1.InitReport

    End Sub

    Private Sub BtnPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnPrint.Click
        'Dim Report As New RptVendor
        'FrmReport.CrystalReportViewer1.ReportSource = Report
        'FrmReport.CrystalReportViewer1.Refresh()
        'FrmReport.ShowDialog()
    End Sub
End Class
