﻿Imports System.Data.Odbc
Module Module1
    Public Function QueryToDataSet(ByVal sql As String) As DataSet
        Dim vKoneksi As OdbcConnection
        vKoneksi = New OdbcConnection("DSN=DSNPROJECT2")
        vKoneksi.Open()
        Dim vCMD As New OdbcCommand
        vCMD = vKoneksi.CreateCommand
        vCMD.CommandText = sql
        Dim vSDA As New OdbcDataAdapter
        vSDA.SelectCommand = vCMD
        Dim ds As New DataSet
        vSDA.Fill(ds)
        vKoneksi.Close()
        Return ds
    End Function

    Public Sub ExecuteQuery(ByVal SQL As String)
        Dim vKoneksi As OdbcConnection
        vKoneksi = New OdbcConnection("DSN=DSNPROJECT2")
        vKoneksi.Open()
        Dim vCMD As New OdbcCommand
        vCMD = vKoneksi.CreateCommand
        vCMD.CommandText = SQL
        vCMD.CommandTimeout = 300
        vCMD.ExecuteNonQuery()
        vKoneksi.Close()
    End Sub
End Module
