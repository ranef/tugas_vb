﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Purchase_order
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.LblPurchaseOrder = New System.Windows.Forms.Label
        Me.LblNoPO = New System.Windows.Forms.Label
        Me.LblTgl = New System.Windows.Forms.Label
        Me.TxtNoPO = New System.Windows.Forms.TextBox
        Me.DtpTgl = New System.Windows.Forms.DateTimePicker
        Me.LblNoPR = New System.Windows.Forms.Label
        Me.LblVendor = New System.Windows.Forms.Label
        Me.TxtNoPR = New System.Windows.Forms.TextBox
        Me.TxtVendor = New System.Windows.Forms.TextBox
        Me.TxtItemCode = New System.Windows.Forms.TextBox
        Me.TxtItemName = New System.Windows.Forms.TextBox
        Me.TxtPrice = New System.Windows.Forms.TextBox
        Me.LblItemCode = New System.Windows.Forms.Label
        Me.LblItemName = New System.Windows.Forms.Label
        Me.LblPrice = New System.Windows.Forms.Label
        Me.LblQty = New System.Windows.Forms.Label
        Me.LblSubtotal = New System.Windows.Forms.Label
        Me.TxtQty = New System.Windows.Forms.TextBox
        Me.TxtSubtotal = New System.Windows.Forms.TextBox
        Me.DgPurchaseOrder = New System.Windows.Forms.DataGridView
        Me.BtnDeleteTransaksi = New System.Windows.Forms.Button
        Me.BtnSave = New System.Windows.Forms.Button
        Me.BtnPrint = New System.Windows.Forms.Button
        Me.BtnNew = New System.Windows.Forms.Button
        Me.LblSubtotalH = New System.Windows.Forms.Label
        Me.LblPPN = New System.Windows.Forms.Label
        Me.LblGrandtotal = New System.Windows.Forms.Label
        Me.TxtSubtotalH = New System.Windows.Forms.TextBox
        Me.TxtPPN = New System.Windows.Forms.TextBox
        Me.TxtGrandtotal = New System.Windows.Forms.TextBox
        Me.BtnNoPO = New System.Windows.Forms.Button
        Me.BtnNoPR = New System.Windows.Forms.Button
        Me.BtnVendor = New System.Windows.Forms.Button
        Me.BtnItemCode = New System.Windows.Forms.Button
        CType(Me.DgPurchaseOrder, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'LblPurchaseOrder
        '
        Me.LblPurchaseOrder.AutoSize = True
        Me.LblPurchaseOrder.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblPurchaseOrder.Location = New System.Drawing.Point(301, 12)
        Me.LblPurchaseOrder.Name = "LblPurchaseOrder"
        Me.LblPurchaseOrder.Size = New System.Drawing.Size(177, 25)
        Me.LblPurchaseOrder.TabIndex = 0
        Me.LblPurchaseOrder.Text = "Purchase Order"
        '
        'LblNoPO
        '
        Me.LblNoPO.AutoSize = True
        Me.LblNoPO.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblNoPO.Location = New System.Drawing.Point(37, 66)
        Me.LblNoPO.Name = "LblNoPO"
        Me.LblNoPO.Size = New System.Drawing.Size(58, 18)
        Me.LblNoPO.TabIndex = 1
        Me.LblNoPO.Text = "No. PO"
        '
        'LblTgl
        '
        Me.LblTgl.AutoSize = True
        Me.LblTgl.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblTgl.Location = New System.Drawing.Point(37, 112)
        Me.LblTgl.Name = "LblTgl"
        Me.LblTgl.Size = New System.Drawing.Size(28, 18)
        Me.LblTgl.TabIndex = 2
        Me.LblTgl.Text = "Tgl"
        '
        'TxtNoPO
        '
        Me.TxtNoPO.Location = New System.Drawing.Point(126, 66)
        Me.TxtNoPO.Name = "TxtNoPO"
        Me.TxtNoPO.Size = New System.Drawing.Size(149, 20)
        Me.TxtNoPO.TabIndex = 3
        '
        'DtpTgl
        '
        Me.DtpTgl.CustomFormat = "dddd,  MMMM dd, yyyy"
        Me.DtpTgl.Location = New System.Drawing.Point(126, 104)
        Me.DtpTgl.Name = "DtpTgl"
        Me.DtpTgl.Size = New System.Drawing.Size(213, 20)
        Me.DtpTgl.TabIndex = 4
        '
        'LblNoPR
        '
        Me.LblNoPR.AutoSize = True
        Me.LblNoPR.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblNoPR.Location = New System.Drawing.Point(419, 73)
        Me.LblNoPR.Name = "LblNoPR"
        Me.LblNoPR.Size = New System.Drawing.Size(61, 18)
        Me.LblNoPR.TabIndex = 5
        Me.LblNoPR.Text = " No. PR"
        '
        'LblVendor
        '
        Me.LblVendor.AutoSize = True
        Me.LblVendor.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblVendor.Location = New System.Drawing.Point(423, 107)
        Me.LblVendor.Name = "LblVendor"
        Me.LblVendor.Size = New System.Drawing.Size(55, 18)
        Me.LblVendor.TabIndex = 6
        Me.LblVendor.Text = "Vendor"
        '
        'TxtNoPR
        '
        Me.TxtNoPR.Location = New System.Drawing.Point(498, 70)
        Me.TxtNoPR.Name = "TxtNoPR"
        Me.TxtNoPR.Size = New System.Drawing.Size(156, 20)
        Me.TxtNoPR.TabIndex = 7
        '
        'TxtVendor
        '
        Me.TxtVendor.Location = New System.Drawing.Point(498, 105)
        Me.TxtVendor.Name = "TxtVendor"
        Me.TxtVendor.Size = New System.Drawing.Size(156, 20)
        Me.TxtVendor.TabIndex = 8
        '
        'TxtItemCode
        '
        Me.TxtItemCode.Location = New System.Drawing.Point(40, 181)
        Me.TxtItemCode.Name = "TxtItemCode"
        Me.TxtItemCode.Size = New System.Drawing.Size(106, 20)
        Me.TxtItemCode.TabIndex = 9
        '
        'TxtItemName
        '
        Me.TxtItemName.Location = New System.Drawing.Point(216, 181)
        Me.TxtItemName.Name = "TxtItemName"
        Me.TxtItemName.ReadOnly = True
        Me.TxtItemName.Size = New System.Drawing.Size(120, 20)
        Me.TxtItemName.TabIndex = 10
        '
        'TxtPrice
        '
        Me.TxtPrice.Location = New System.Drawing.Point(342, 181)
        Me.TxtPrice.Name = "TxtPrice"
        Me.TxtPrice.ReadOnly = True
        Me.TxtPrice.Size = New System.Drawing.Size(114, 20)
        Me.TxtPrice.TabIndex = 11
        '
        'LblItemCode
        '
        Me.LblItemCode.AutoSize = True
        Me.LblItemCode.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblItemCode.Location = New System.Drawing.Point(55, 157)
        Me.LblItemCode.Name = "LblItemCode"
        Me.LblItemCode.Size = New System.Drawing.Size(76, 18)
        Me.LblItemCode.TabIndex = 12
        Me.LblItemCode.Text = "Item Code"
        '
        'LblItemName
        '
        Me.LblItemName.AutoSize = True
        Me.LblItemName.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblItemName.Location = New System.Drawing.Point(241, 157)
        Me.LblItemName.Name = "LblItemName"
        Me.LblItemName.Size = New System.Drawing.Size(80, 18)
        Me.LblItemName.TabIndex = 13
        Me.LblItemName.Text = "Item Name"
        '
        'LblPrice
        '
        Me.LblPrice.AutoSize = True
        Me.LblPrice.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblPrice.Location = New System.Drawing.Point(379, 157)
        Me.LblPrice.Name = "LblPrice"
        Me.LblPrice.Size = New System.Drawing.Size(42, 18)
        Me.LblPrice.TabIndex = 14
        Me.LblPrice.Text = "Price"
        '
        'LblQty
        '
        Me.LblQty.AutoSize = True
        Me.LblQty.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblQty.Location = New System.Drawing.Point(505, 157)
        Me.LblQty.Name = "LblQty"
        Me.LblQty.Size = New System.Drawing.Size(31, 18)
        Me.LblQty.TabIndex = 15
        Me.LblQty.Text = "Qty"
        '
        'LblSubtotal
        '
        Me.LblSubtotal.AutoSize = True
        Me.LblSubtotal.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblSubtotal.Location = New System.Drawing.Point(619, 157)
        Me.LblSubtotal.Name = "LblSubtotal"
        Me.LblSubtotal.Size = New System.Drawing.Size(62, 18)
        Me.LblSubtotal.TabIndex = 16
        Me.LblSubtotal.Text = "Subtotal"
        '
        'TxtQty
        '
        Me.TxtQty.Location = New System.Drawing.Point(461, 181)
        Me.TxtQty.Name = "TxtQty"
        Me.TxtQty.Size = New System.Drawing.Size(121, 20)
        Me.TxtQty.TabIndex = 17
        '
        'TxtSubtotal
        '
        Me.TxtSubtotal.Location = New System.Drawing.Point(588, 181)
        Me.TxtSubtotal.Name = "TxtSubtotal"
        Me.TxtSubtotal.ReadOnly = True
        Me.TxtSubtotal.Size = New System.Drawing.Size(121, 20)
        Me.TxtSubtotal.TabIndex = 18
        '
        'DgPurchaseOrder
        '
        Me.DgPurchaseOrder.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DgPurchaseOrder.Location = New System.Drawing.Point(40, 219)
        Me.DgPurchaseOrder.Name = "DgPurchaseOrder"
        Me.DgPurchaseOrder.Size = New System.Drawing.Size(669, 187)
        Me.DgPurchaseOrder.TabIndex = 19
        '
        'BtnDeleteTransaksi
        '
        Me.BtnDeleteTransaksi.Location = New System.Drawing.Point(156, 480)
        Me.BtnDeleteTransaksi.Name = "BtnDeleteTransaksi"
        Me.BtnDeleteTransaksi.Size = New System.Drawing.Size(103, 23)
        Me.BtnDeleteTransaksi.TabIndex = 23
        Me.BtnDeleteTransaksi.Text = "Delete Transaksi"
        Me.BtnDeleteTransaksi.UseVisualStyleBackColor = True
        '
        'BtnSave
        '
        Me.BtnSave.Location = New System.Drawing.Point(156, 434)
        Me.BtnSave.Name = "BtnSave"
        Me.BtnSave.Size = New System.Drawing.Size(103, 23)
        Me.BtnSave.TabIndex = 22
        Me.BtnSave.Text = "Save"
        Me.BtnSave.UseVisualStyleBackColor = True
        '
        'BtnPrint
        '
        Me.BtnPrint.Location = New System.Drawing.Point(39, 480)
        Me.BtnPrint.Name = "BtnPrint"
        Me.BtnPrint.Size = New System.Drawing.Size(98, 23)
        Me.BtnPrint.TabIndex = 21
        Me.BtnPrint.Text = "Print"
        Me.BtnPrint.UseVisualStyleBackColor = True
        '
        'BtnNew
        '
        Me.BtnNew.Location = New System.Drawing.Point(39, 434)
        Me.BtnNew.Name = "BtnNew"
        Me.BtnNew.Size = New System.Drawing.Size(98, 23)
        Me.BtnNew.TabIndex = 20
        Me.BtnNew.Text = "New"
        Me.BtnNew.UseVisualStyleBackColor = True
        '
        'LblSubtotalH
        '
        Me.LblSubtotalH.AutoSize = True
        Me.LblSubtotalH.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblSubtotalH.Location = New System.Drawing.Point(458, 439)
        Me.LblSubtotalH.Name = "LblSubtotalH"
        Me.LblSubtotalH.Size = New System.Drawing.Size(73, 18)
        Me.LblSubtotalH.TabIndex = 24
        Me.LblSubtotalH.Text = "SubtotalH"
        '
        'LblPPN
        '
        Me.LblPPN.AutoSize = True
        Me.LblPPN.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblPPN.Location = New System.Drawing.Point(458, 468)
        Me.LblPPN.Name = "LblPPN"
        Me.LblPPN.Size = New System.Drawing.Size(39, 18)
        Me.LblPPN.TabIndex = 25
        Me.LblPPN.Text = "PPN"
        '
        'LblGrandtotal
        '
        Me.LblGrandtotal.AutoSize = True
        Me.LblGrandtotal.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblGrandtotal.Location = New System.Drawing.Point(458, 501)
        Me.LblGrandtotal.Name = "LblGrandtotal"
        Me.LblGrandtotal.Size = New System.Drawing.Size(77, 18)
        Me.LblGrandtotal.TabIndex = 26
        Me.LblGrandtotal.Text = "Grandtotal"
        '
        'TxtSubtotalH
        '
        Me.TxtSubtotalH.Location = New System.Drawing.Point(550, 437)
        Me.TxtSubtotalH.Name = "TxtSubtotalH"
        Me.TxtSubtotalH.ReadOnly = True
        Me.TxtSubtotalH.Size = New System.Drawing.Size(159, 20)
        Me.TxtSubtotalH.TabIndex = 27
        '
        'TxtPPN
        '
        Me.TxtPPN.Location = New System.Drawing.Point(550, 466)
        Me.TxtPPN.Name = "TxtPPN"
        Me.TxtPPN.ReadOnly = True
        Me.TxtPPN.Size = New System.Drawing.Size(159, 20)
        Me.TxtPPN.TabIndex = 28
        '
        'TxtGrandtotal
        '
        Me.TxtGrandtotal.Location = New System.Drawing.Point(550, 499)
        Me.TxtGrandtotal.Name = "TxtGrandtotal"
        Me.TxtGrandtotal.ReadOnly = True
        Me.TxtGrandtotal.Size = New System.Drawing.Size(159, 20)
        Me.TxtGrandtotal.TabIndex = 29
        '
        'BtnNoPO
        '
        Me.BtnNoPO.Location = New System.Drawing.Point(281, 66)
        Me.BtnNoPO.Name = "BtnNoPO"
        Me.BtnNoPO.Size = New System.Drawing.Size(58, 23)
        Me.BtnNoPO.TabIndex = 30
        Me.BtnNoPO.Text = "..."
        Me.BtnNoPO.UseVisualStyleBackColor = True
        '
        'BtnNoPR
        '
        Me.BtnNoPR.Location = New System.Drawing.Point(660, 68)
        Me.BtnNoPR.Name = "BtnNoPR"
        Me.BtnNoPR.Size = New System.Drawing.Size(58, 23)
        Me.BtnNoPR.TabIndex = 31
        Me.BtnNoPR.Text = "..."
        Me.BtnNoPR.UseVisualStyleBackColor = True
        '
        'BtnVendor
        '
        Me.BtnVendor.Location = New System.Drawing.Point(660, 101)
        Me.BtnVendor.Name = "BtnVendor"
        Me.BtnVendor.Size = New System.Drawing.Size(58, 23)
        Me.BtnVendor.TabIndex = 32
        Me.BtnVendor.Text = "..."
        Me.BtnVendor.UseVisualStyleBackColor = True
        '
        'BtnItemCode
        '
        Me.BtnItemCode.Location = New System.Drawing.Point(152, 178)
        Me.BtnItemCode.Name = "BtnItemCode"
        Me.BtnItemCode.Size = New System.Drawing.Size(58, 23)
        Me.BtnItemCode.TabIndex = 33
        Me.BtnItemCode.Text = "..."
        Me.BtnItemCode.UseVisualStyleBackColor = True
        '
        'Purchase_order
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(745, 545)
        Me.Controls.Add(Me.BtnItemCode)
        Me.Controls.Add(Me.BtnVendor)
        Me.Controls.Add(Me.BtnNoPR)
        Me.Controls.Add(Me.BtnNoPO)
        Me.Controls.Add(Me.TxtGrandtotal)
        Me.Controls.Add(Me.TxtPPN)
        Me.Controls.Add(Me.TxtSubtotalH)
        Me.Controls.Add(Me.LblGrandtotal)
        Me.Controls.Add(Me.LblPPN)
        Me.Controls.Add(Me.LblSubtotalH)
        Me.Controls.Add(Me.BtnDeleteTransaksi)
        Me.Controls.Add(Me.BtnSave)
        Me.Controls.Add(Me.BtnPrint)
        Me.Controls.Add(Me.BtnNew)
        Me.Controls.Add(Me.DgPurchaseOrder)
        Me.Controls.Add(Me.TxtSubtotal)
        Me.Controls.Add(Me.TxtQty)
        Me.Controls.Add(Me.LblSubtotal)
        Me.Controls.Add(Me.LblQty)
        Me.Controls.Add(Me.LblPrice)
        Me.Controls.Add(Me.LblItemName)
        Me.Controls.Add(Me.LblItemCode)
        Me.Controls.Add(Me.TxtPrice)
        Me.Controls.Add(Me.TxtItemName)
        Me.Controls.Add(Me.TxtItemCode)
        Me.Controls.Add(Me.TxtVendor)
        Me.Controls.Add(Me.TxtNoPR)
        Me.Controls.Add(Me.LblVendor)
        Me.Controls.Add(Me.LblNoPR)
        Me.Controls.Add(Me.DtpTgl)
        Me.Controls.Add(Me.TxtNoPO)
        Me.Controls.Add(Me.LblTgl)
        Me.Controls.Add(Me.LblNoPO)
        Me.Controls.Add(Me.LblPurchaseOrder)
        Me.Name = "Purchase_order"
        Me.Text = "Form_purchase_order"
        CType(Me.DgPurchaseOrder, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents LblPurchaseOrder As System.Windows.Forms.Label
    Friend WithEvents LblNoPO As System.Windows.Forms.Label
    Friend WithEvents LblTgl As System.Windows.Forms.Label
    Friend WithEvents TxtNoPO As System.Windows.Forms.TextBox
    Friend WithEvents DtpTgl As System.Windows.Forms.DateTimePicker
    Friend WithEvents LblNoPR As System.Windows.Forms.Label
    Friend WithEvents LblVendor As System.Windows.Forms.Label
    Friend WithEvents TxtNoPR As System.Windows.Forms.TextBox
    Friend WithEvents TxtVendor As System.Windows.Forms.TextBox
    Friend WithEvents TxtItemCode As System.Windows.Forms.TextBox
    Friend WithEvents TxtItemName As System.Windows.Forms.TextBox
    Friend WithEvents TxtPrice As System.Windows.Forms.TextBox
    Friend WithEvents LblItemCode As System.Windows.Forms.Label
    Friend WithEvents LblItemName As System.Windows.Forms.Label
    Friend WithEvents LblPrice As System.Windows.Forms.Label
    Friend WithEvents LblQty As System.Windows.Forms.Label
    Friend WithEvents LblSubtotal As System.Windows.Forms.Label
    Friend WithEvents TxtQty As System.Windows.Forms.TextBox
    Friend WithEvents TxtSubtotal As System.Windows.Forms.TextBox
    Friend WithEvents DgPurchaseOrder As System.Windows.Forms.DataGridView
    Friend WithEvents BtnDeleteTransaksi As System.Windows.Forms.Button
    Friend WithEvents BtnSave As System.Windows.Forms.Button
    Friend WithEvents BtnPrint As System.Windows.Forms.Button
    Friend WithEvents BtnNew As System.Windows.Forms.Button
    Friend WithEvents LblSubtotalH As System.Windows.Forms.Label
    Friend WithEvents LblPPN As System.Windows.Forms.Label
    Friend WithEvents LblGrandtotal As System.Windows.Forms.Label
    Friend WithEvents TxtSubtotalH As System.Windows.Forms.TextBox
    Friend WithEvents TxtPPN As System.Windows.Forms.TextBox
    Friend WithEvents TxtGrandtotal As System.Windows.Forms.TextBox
    Friend WithEvents BtnNoPO As System.Windows.Forms.Button
    Friend WithEvents BtnNoPR As System.Windows.Forms.Button
    Friend WithEvents BtnVendor As System.Windows.Forms.Button
    Friend WithEvents BtnItemCode As System.Windows.Forms.Button

End Class
