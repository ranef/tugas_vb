﻿Public Class FrmLookup

    Private Sub BtnPilih_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnPilih.Click
        If Me.Text = "vendor" Then
            FrmVendor.TxtBoxVendorCode.Text = Dg.Item(0, Dg.CurrentRow.Index).Value
            FrmVendor.TxtBoxVendorName.Text = Dg.Item(1, Dg.CurrentRow.Index).Value

        End If
        If Me.Text = "po" Then
            Purchase_order.TxtNoPO.Text = Dg.Item(0, Dg.CurrentRow.Index).Value
            Purchase_order.DtpTgl.Value = Dg.Item(2, Dg.CurrentRow.Index).Value
            Purchase_order.TxtNoPR.Text = Dg.Item(1, Dg.CurrentRow.Index).Value
            Purchase_order.TxtVendor.Text = Dg.Item(3, Dg.CurrentRow.Index).Value
            Purchase_order.TxtSubtotalH.Text = Dg.Item(4, Dg.CurrentRow.Index).Value
            Purchase_order.TxtGrandtotal.Text = Dg.Item(6, Dg.CurrentRow.Index).Value
        End If
        If Me.Text = "m_item" Then
            Frm_item.Txt_item_code.Text = Dg.Item(0, Dg.CurrentRow.Index).Value
            Frm_item.Txt_item_name.Text = Dg.Item(1, Dg.CurrentRow.Index).Value
            Frm_item.Txt_price.Text = Dg.Item(2, Dg.CurrentRow.Index).Value
        End If
        If Me.Text = "p_item" Then
            Purchase_order.TxtItemCode.Text = Dg.Item(0, Dg.CurrentRow.Index).Value
            Purchase_order.TxtItemName.Text = Dg.Item(1, Dg.CurrentRow.Index).Value
            Purchase_order.TxtPrice.Text = Dg.Item(2, Dg.CurrentRow.Index).Value
        End If
        If Me.Text = "customer" Then
            FrmKasir.TxtCustomerCode.Text = Dg.Item(0, Dg.CurrentRow.Index).Value
            FrmKasir.TxtCustomerName.Text = Dg.Item(1, Dg.CurrentRow.Index).Value
        End If
        If Me.Text = "item" Then
            FrmKasir.TxtItemCode.Text = Dg.Item(0, Dg.CurrentRow.Index).Value
            FrmKasir.TxtItemName.Text = Dg.Item(1, Dg.CurrentRow.Index).Value
            FrmKasir.TxtPrice.Text = Dg.Item(2, Dg.CurrentRow.Index).Value
        End If
        If Me.Text = "cari struk" Then
            FrmKasir.TxtNoStruk.Text = Dg.Item(0, Dg.CurrentRow.Index).Value
        End If
        Close()
    End Sub

    Private Sub FrmLookup_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub

    Private Sub BtnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnClose.Click
        Close()
    End Sub

    Private Sub DgFrmLookup_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles Dg.CellContentClick

    End Sub
End Class